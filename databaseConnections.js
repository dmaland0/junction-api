/****************************************
*****************************************
Dependencies*/

//Builtin And Third-Party Modules

//Custom Modules
const MemoryBaseConnector = require('./coreLogic/services/memoryBaseConnector').MemoryBaseConnector
const environmentVariables = require("./environmentVariables").environmentVariables;

/****************************************
*****************************************
Module Logic*/

exports.connections = {};

if (environmentVariables.useEmbeddedMemoryBase) {
  exports.connections.example = new MemoryBaseConnector({fileNameBase: "example", connectionName: "example", writeFrequency: 30000});
}
