# Junction-API

Junction-API is a web-application programming framework. It's an offshoot of 2CubicAPI https://gitlab.com/dmaland0/2cubicapi which was itself forked from CubicAPI https://gitlab.com/dmaland0/cubicAPI (which came from OpenCubes https://gitlab.com/openCubes). Junction-API is meant to make creating applications as "frictionless" as practicable, by way of being self-contained and uncomplicated whenever possible. The main contrast of Junction-API over its predecessors is to have no dependencies "out of the box." That is, you should be able to clone down the repo and start the server with no intermediate steps.

You can, of course, use this system to serve a simple website without any "application" functionality at all. This software can certainly not lay claim to being a full replacement for Apache, Nginx, IIS, or any other web server, but it can be an entirely workable alternative in some contexts.

---

---

## Installation

### Basics

1. Ensure that Node.js and NPM (or Yarn) are installed on the execution platform of your choice. An LTS release of Node is strongly recommended for the best chance of a quick, successful start.

    * Because file paths are highly reliant on where your system thinks you are in the filesystem when starting the server, it is very strongly recommended to use `npm run [command]` or `yarn [command]` as the invocation (or use the invocations shown in package.json directly, from the project root). 

2. Find the place in your filesystem where you want the code directory to reside, and then clone down the repository.

    * Unless you are developing directly for Junction-API, you should throw out the .git directory and run `git init` to start a new project. Remember to review the various .gitignore files to make sure they make sense for you!

3. `EnvironmentVariables.js` needs to be present in the project root and contain appropriate values for your application. `EnvironmentVariablesExample.js` is provided as a starting point:

    * The `insecureRequestPort` value is where you expect a regular HTTP request to arrive. These requests receive a 307 redirect to the requested host, but at the secure `serverPort`. If the port is already in use by the machine (real, virtual, docker container, etc.), then you will get an `EACCESS` exception and a crash. Note that if you want to use port 80 (the conventional port for HTTP), you will likely need to have root/ system superuser/ system administrator level access on the executing machine.

    * The `serverPort` value is where the secure server listens. Like `insecureRequestPort`, a value already in use by the execution platform will cause Node to throw an exception and exit. Note that if you want to use port 443 (the conventional port for HTTPS), you will likely need to have root/ system superuser/ system administrator level access on the executing machine.

    * Use `cacheTimeInSeconds` to determine the `max-age` cache control value sent in responses. The value 604800 is the number of seconds that constitutes one week.

    * You can turn request/ response logging on and off with `keepLogs`.

    * `allowMultiprocessing` is the setting which controls whether the server will try to create a separate process for each CPU available to the execution platform. With no modification, this setting can't actually be enabled, as the built-in MemoryBase database functionality doesn't work properly with multiprocessing. If MemoryBase is used for anything, the multiprocess setup is bypassed. When MemoryBase is not in use, setting this to `true` is helpful when you actually do need to maximize the execution performance of your application, but it can make debugging difficult: Each process gets duplicate execution code, and determining which process will consume a given request is non-trivial. As such, you should probably refrain from enabling this until you have a stable application...and then, be sure that you beta-test carefully!

    * The `httpsKey` value is any valid path to a key file usable for secure requests. For instance, with Let's Encrypt, the path might be something like `/etc/letsencrypt/live/yourdomain/privkey.pem`. *Relative paths use the execution directory as the reference point. If you use npm `commmand` or yarn `command` for execution, the execution directory is the project root.*

        * Also - new versions of Junction-API feature a built-in key and certificate file that can help you get rolling quickly without generating or obtaining those files yourself. A major caveat, though, is that you should NOT use the built-ins for production. Because the key is available to everyone via the Junction-API repository, the certificate provides no real security.

    * Similarly, `httpsCertificate` is any valid path to a certificate, relative to the execution directory.

    * `httpsChain` is an array of valid paths to files that establish a certificate chain. In some cases, a "fullchain" certificate might be pre-generated, meaning that the array need only have one path string.

      * A quick note about files related to https: On some systems with OpenSSL insalled (like certain Ubuntu machines), you can generate a key and certificate by navigating a terminal to the https directory and executing: `openssl req -newkey rsa:2048 -nodes -keyout key.pem -x509 -days 365 -out certificate.pem`. Please note that, on Windows, using a CLI other than CMD or PowerShell may result in output that doesn't work properly. There will be some prompts for you to follow. During initial development and testing, you will very likely be running with self-signed certificates. Modern browsers are (justifiably) picky about allowing this. Bypassing the warnings and/ or allowing your testing certificate in any particular browser is beyond the scope of this document.

    * The `defaultfile` entry allows you to specify which file in the "ui" folder is served for a GET request to the default route, "/". This is NOT a file path - it is a file name only, because the target directories (`ui` and `./coreLogic/basicUI`) are already known.

    * `handlesSubdomains` allows you to specify whether Junction-API can redirect file requests to different UI subdirectories based on the request subdomain. If true, regular file requests from browsers will have their paths transformed to use the "ui" sub-directory matching the domain or subdomain found on the request.

    * The `registrationDisabled` boolean allows you to prevent unauthenticated sessions from creating new users. (Superusers are NOT affected by this.) This can be handy for security or testing.

    * `maxFailedLoginsBeforeThrottle` allows the setting of the login throttler's "strictness."

    * `throttledLoginTimeout` is the number of minutes that login throttling remains in effect after the `maxFailedLoginsBeforeThrottle` number of login attempts is reached.

    * You can use `uploadPath` to set a default location for `uploadController` to write files into. The path is relative to the execution directory, and should end in a forward slash ("/"). The example value is the same as the default that `uploadController` uses if the variable is unset.

    * `useEmbeddedMemoryBase` is a boolean which determines whether to bootstrap the MemoryBase code loated in `coreLogic`. If you're absolutely sure you'll be using another database system, you can set this to `false`.

    * `loginModel` is an object which enumerates how users authenticate themselves.
    
        * The default model is now `keyfile`, where a user essentially uploads a file - that only they should know is tied to their account - in order to prove that they are themselves. Key files can, in theory, be improvements over passwords, because it is extremely hard to "guess" a very specific, say, jpg file. (In any case, a brute-force attack is made much harder, though not impossible.) The user database still uses the `hashedPassword` field. The stored hash is simply that of the keyfile. The `keyfile` model dispenses with registration confirmations and device authorizations, which are a bit unwieldy if your server doesn't easily send emails.

        * The `password` model is, as you might expect, one where the user types in a passphrase that ties to their account. Because passwords can be relatively easy to duplicate, generate, or guess, registration confirmations and device authorizations are required.

    * `allowAnonymousWebsocketConnections` is a boolean which determines whether valid authentication is required to maintain a persistent connection with the Websocket server. This is false by default, in order to avoid a situation where server CPU and bandwidth is being used to maintain communications (even if those are simple pings) with unknown hosts. In some cases, of course, you might wish to avoid this restriction.

    * `mimeTypes` is an array of arrays, each array storing a relevant file extension at index 0, and the corresponding content-type at index 1. If the system doesn't deliver a certain type of content correctly, adding to this list is usually the first place to start in correcting the problem. Remember to append `;base64` to MIME types that should be transferred as binary files, like images and fonts!

    * `allowedExternalOrigins` is an array of strings that specify addresses allowed for cross origin resource sharing. (This is very important if you intend to serve your UI separately from an API.) To work, any specified external origin must be listed in EXACTLY the way it will appear in an origin header sent by a client. If the origin header matches a string in `allowedExternalOrigins`, then appropriate response `Access-Control` headers are sent and the request should complete normally. If an unknown external origin makes a request, no `Access-Control` response headers are sent, preventing the request from completing.

5. Once everything is ready, you can invoke `npm run` from the `coreLogic` directory to manually start the server. The command `npm run debug` is for development support. It uses `nodemon` to watch for file changes, and the `--inspect` flag to allow a live debugger to attach. Of course, this doesn't work for running a server in production. For that, pm2 is recommended.

    * First, make sure that npm is installed on the server, if it isn't already.

    * Next, install pm2. The invocation is likely something similar to `npm install pm2 -g`, but the foregoing isn't guaranteed to work forever or in all situations.

    * With pm2 installed, the easiest way to start the server is to use the "start any application" behavior. First, navigate to the root directory of the project where `package.json` resides. Then, use the command which (at the time of this writing) looks like `pm2 start "npm run start" --name examplename`. This causes pm2 to request that npm run the start script defined in `package.json`, and then assign the name "examplename" to the running process for quick access.

### The "coreLogic" Directory (And Upgrades)

`CoreLogic` is the directory housing the code that forms the heart of Junction-API. Keeping the code here makes upgrades easy: Just download the latest changes from the repository, and then replace the old `coreLogic` directory with the new one. Of course, this means that you should NOT edit the contents of `coreLogic` unless you're willing to deal with losing your changes due to an upgrade. Please do remember to make a backup, whatever the case!

### The "ui" Directory

The folder called "ui" is so named because it is meant to house the files used to create a user interface. It's the equivalent of "public" on other web server applications. As such, you should carefully refrain from putting ANYTHING in `ui` which you don't want everybody able to connect to the server to be able to read. Some files can be restricted to logged-in users, but you still need to be careful.

If no HTML files are found in the `ui` directory, then Junction-API will attempt to serve file requests from `coreLogic/basicUI`. However, if ANY files with the .html extension are found in `ui`, no attempt will be made to serve the basic UI. You can, of course, copy the contents of `coreLogic/basicUI` to the `ui` directory and work from there if you like.

### Subdomains

As was presented in the discussion of `environmentVariables.js`, Junction-API can handle subdomains. This is currently done through the presentation of different UI elements. When subdomains are allowed, the server expects that the `ui` directory has internal directories with names matching the subdomain to act as the root of each subdomain's UI.

The primary domain is itself treated as a subdomain. For instance, "domain.com" is interpreted as being the subdomain "domain," so a `domain` subdirectory would need to be present in the `ui` directory.

### API-Style File Paths

Requests for files that lack a recognized extension from environmentVariables.mimeTypes will be searched for as though they were HTML files. This allows a browser to request an HTML file as something like /example. Internally, the server turns the path into /example.html, and tries to send back the file.

This can, of course, cause unexpected behavior if you're trying to use files without extensions, or files with extensions not registered in environmentVariables.mimeTypes.

### "ManagedUIFile" Functionality

"ManagedUIFile" refers to the server's ability to serve text files from a database. For instance, HTML templates could be stored in the table, ready to be easily edited by users with access to the system.

When given a file request, Junction-API will always attempt to serve the file from the regular filesystem. If the file read fails, the server will then attempt to find an entry in the main database with a matching name to the original request. If file content is found, it is retrieved and processed like any other file.

You should consider ALL managed UI file entries to be public, so take care not to put any secrets in them!

### Logging

Junction-API tracks request and response activity in log files that reside in `/serverLogs/`. These files can potentially aid in debugging, addressing security issues, and in usage analytics. In an umodified install, this is just JSON stored in cleartext.

You'll also probably notice a root-level file called `logEmails` at some point. By default, Junction-API has no way of sending actual emails to an email server, so user registration confirmations, password reset codes, and device authorization messages will end up here. They can be sent along by hand until your preferred email solution can be wired up.

---

---

## Development, Part 1

#### A Special Note: The Default Superuser

The first user to register on a Junction-API system is always given superuser privileges. This aids in development and deployment, but it's also a minor security risk that you should be aware of. For maximum safety, set up a trusted superuser in the database before real deployment takes place.

### The Basic Rules Of Junction-API

Junction-API, like many other application frameworks and foundations, has a certain level of opinionation. The hope is that this opinionation isn't too onerous, while still enforcing a certain "focus."

"Out of the box" and unmodified, this API system...

- Only recognizes GET, POST, PUT, DELETE, and OPTIONS verbs.

- Requires valid JSON for POST and PUT verbs as the only acceptable payload (even for file uploads). No other format will be consumed.

- Leans heavily toward a design pattern such that an API route is named after the object to be retrieved, created, or modified, and that a parameter can be supplied for identifying a resource. For instance:
    + GET `/api/users/` Retrieves a list of users.
    + GET `/api/users/1` Retrieves the user with the ID "1."
    + POST `/api/users/` Creates a new user.
    + PUT `/api/users/1` Modified the user with the ID "1."
    + DELETE `/api/users/1` Removes the user with the ID "1."

- Operates such that the `requiredFields` array does not prevent a request from having more than is required, but rather defines the "minimum" fields necessary for a request to be considered valid.

#### File Uploads

Because Junction-API only recognizes JSON as being valid for incoming data, file uploads may be a bit different than some developers are used to. The key to success is supplying the file contents as a base64 encoded string. A minimal but functional example of upload functionality can be found via `coreLogic/basicUI/uploads.html` and the scripts it utilizes. The `uploadController` in `coreLogic` is itself fairly minimal, but does check filenames to avoid overwrites.

### Regarding Overrides and Changes To Default Behaviors

Because Junction-API is rather bare-bones, you may wish to override or change the way certain built-in features work. For instance, you might want to extend the deletion of managedUIFiles to a User with a role (or group membership) like "File Managers." This poses a problem, though, because to do so you'd have to edit pieces of Junction-API's core. The structure of the User object would have to be changed, and the core route for deleting managedUIFiles would have to be modified. The change might cause problems with other core features, and an upgrade would potentially wipe out your modifications.

The solution to this problem is rather simple: Add your own dataObjects and routes, while leaving the core untouched.

What this means in practice, for the purposes of the example, is threefold:

First, you need to define your own dataObject that's called, say, `UserRoles`, which references User datatObject IDs to map new information onto the core User model. For instance, userRole 1 might have a field `roles`, with a value that's an array of role names, and another field `user` which is a user ID expected to be found in the core User's table.

Second, you would need to create a controller that determines if a User is a file manager, first by looking up the acting User via their token, then looking up the UserRole corresponding to that User.

Third, you would create your own delete route (in the root-level `routeDefinitions` file) that could take the form:

```Javascript
    route: "customManagedUIFiles/?",
    beforeAction: [usersController.authenticateWithToken, customManagedUIFilesController.requireFileManager],
    action: managedUIFilesController.delete,
    description: "Remove a managed UI file. The parameter is the name of the file you wish to change. Deletion is permanent and can't be undone. Unpublishing a file is much safer. Restricted to file managers."
```

In the end, you get the best of both worlds. You don't have to edit anything in coreLogic, but can still leverage some of its functionality while getting exactly what you want. If you upgrade coreLogic later, your custom functionality isn't changed or removed.

### RouteDefinitions.js

In the project root, you will find `routeDefinitions.js`, which is where you specify what a request to a certain URL does. Routes can respond to GET, POST, PUT, and DELETE verbs. A separate array of routes is available for each possible verb.

Route definitions rely on controllers to work, so each controller necessary should be required in the `Dependencies` section. For example:

`const someController = require("./controllers/someController");`

Or, to utilize the `usersController` from coreLogic:

`const usersController = require("./coreLogic/controllers/usersController");`

Each of the route arrays mentioned is an array of objects. A route object might look like this:

```Javascript
{
    route: "path/?",
    beforeAction: [anotherController.function],
    action: thingsController.function
    requiredFields: ["aField", "anotherField", ...]
}
```

A "?" character in the `route` field is a parameter that will match anything at that position in the request URL. Other strings must match exactly - so, in the example case, `path/8` and `path/ridiculousParameter` should work, but `poth/0` (a misspelling) will fail.

The `beforeAction` array is a collection of functions that should be executed in order before `action`. If any `beforeAction` reports failure, the chain of action executions ends, and the server responds with an error. The `beforeAction` array is an important part of staying "DRY" (Don't Repeat Yourself). If you find yourself duplicating an action over and over as a preamble to other API functionality, it might save you time and effort to separate that functionality and call it via the `beforeAction` chain. (A prime example of this is making sure a user is authenticated before allowing an action, which is why you'll see `beforeAction: [usersController.authenticateWithToken]` everywhere if you examine `./coreLogic/routeDefinitions.js`.

The `action` is the core controller and function that should be called when the route is matched.

The array of `requiredFields` is helpful for routes that accept data. If all the fields aren't present in the request, the router will reject the request without calling the controller, and send back a message to help with figuring out what was missing. Note that there is no check as to whether the fields contain empty or "falsey" values; The existence of the field is all that's necessary.

#### An Important Note On Actions

With Junction-API, all actions take the same form. Any action is just a function exported from a Node module that's available to `routeDefinitions.js`. Actions MUST be of this form to work correctly:

```Javascript
function (callObject, resultCallback) {
    //Statements to execute, which can use data from callObject...

    //Then in an appropriate place, perhaps within another callback:
    resultCallback(successBoolean, httpCodeInteger, [newCookieString, anotherNewCookieString, etc.], messageString, dataJSONString);
}
```

The idea here is that any action can (theoretically) chain into any other action. The `resultCallback` is not actually an HTTP response, but an abstraction of one. This means that controller functionality is more reusable across different controllers: A real response to a request isn't sent until the `resultCallback` originally passed by `router.js` is actually invoked...and `router.js` calls back to `server.js`. Thus, any number of call/ callback cycles can be run by other modules until the `resultCallback` "owned" by `router.js` ends up triggering an HTTP response.

`CallObject` is data for the purpose of future-proofing: If `router.js` adds something to `callObject` for new actions, existing actions simply ignore the new information without having to be modified.

### /api/documentation/

This hardcoded route available sends a cleaned-up `routeDefinitions.js` back to the requester as a JSON object. This is a way for Junction-API to self-document and make development easier. Do remember, though, that this means your `routeDefinitions.js` file should be considered public. As such, don't put anything in it that should remain secret!

### apiDocumentation.html

This file amounts to a "user-friendly" version of `/api/documentation`, rendered out in an easy to read way with auto-generated forms so that most API interactions can be tested.

In certain cases, the default `Include()` paths may need to be modified to accommodate a different directory structure. If apiDocumentation.html doesn't seem to work, looking for and correcting any errors related to the `Include()` tags is an advisable starting place.

### JITRender Tags

"JIT" stands for "Just In Time." When one of these tags is encountered, for example, `<JITRenderServerTime>` a replacement process occurs:

* The replacement information is stored or generated in a Node module (`justInTimeRenderingActions.js`), by way of function calls that are made to correspond to the second part of the `<JITRender...>` tag and...

* The replacement information is passed as the single parameter to a callback.

* The timing of the replacement is just before the server responds with the file; The value is not pre-rendered.

An important thing to remember about using function output is that `fileController.js` calls the functions in a way that's a sort of "naive, brute-force." No matter what the function is, it's fed two parameters, which are currently assumed to be an object (with HTTP header information and the incoming HTTP request itself), plus the callback function. 

### Include() Tags

Include() tags allow for files to be easily imported into other files. The parentheses are where the file path is specified, for instance `<Include(snippet.html)>`. File includes are always relative to the `ui/` directory...unless the server handles subdomains, which means that the includes are relative to the `ui/subdomain` directory. Managed UI files can be used as well, of course.

Included files can themselves include other files.

### RequiresLogin Tags

RequiresLogin tags prevent Junction-API from rendering a text file if the requester is unable to be authenticated.

---

---

## Websockets

A new feature of Junction-API is the ability to upgrade an existing connection to a Websocket.

When the Websocket connection is made, the server attempts to authenticate the user with the HTTP headers (including cookies) available at the time of the connection upgrade. By default `allowAnonymousWebsocketConnections` in `environmentVariables` is false, so a user who isn't yet logged in will be immediately disconnected. A user who has logged in normally will have their user ID and superuser true/ false status set on the socket object itself, accessible at `socket.user`. The current behavior is to pass the socket object to `webSocketActions`, so that user data is accessible to other logic.

Because Websockets don't behave like traditional requests, they aren't "routed." Instead, an incoming message to `webSocketActions` is a request object with fields for an action, and an array of parameters. An example might look like this, which is a request to set a certain time between server pings:

```Javascript
let message = {
        request: {
            action: "setPing",
            parameters: [1000]
        }
    }
```

This message construction assumes that you're using the `socketConnector.js` script found in `coreLogic/basicUI/scripts` as a foundation. The `socketConnector.js` script is loaded on the default login page, if you want to see it in action immediately without having to build anything of your own.

Another built-in action is `listAllActions,` which is intended to catalog all available WebSocket actions available on the server.

The Websocket server allows clients to request a custom ping time, as long as the requested time is greater than 10 seconds. `socketCommunication.setPing` in `socketConnector.js` is a fast and convenient way to accomplish this.

### Constructing Your Own Actions

The built-in `webSocketActions` logic is part of `coreLogic`, so it shouldn't be edited under normal circumstances. Instead, use the root-level `webSocketActions` to construct your Websocket behaviors. An example action is provided to help get you started. When the server starts, the actions you provide and the core actions are merged, with any action name conflicts being noted in the console.

Your list of Websocket actions is an object, itself containing other objects that describe your actions and provide their execution logic. For example:

```Javascript
let actions = {
    setPing: {
        description: `Request a server ping interval for a WebSocket connection. Parameters array [0] is the desired ping time in milliseconds.`,
        execution: require("./services/setPing").setPing
    },
    listAllActions: {
        description: `Request a list of all actions supported by the server.`,
        execution: listAllActions
    }
};
```

The pattern for execution logic is a function that takes the communicating socket, the incoming message from the socket, a responseObject, and a callback as parameters. The callback should be executed when you want to communicate with the socket, and the callback function itself takes the socket and the responseObject as parameters.

For example:

```Javascript
function exampleFunction (socket, message, responseObject, callback) {

    if (message.request.parameters[0]) {

        responseObject.success = true;
        responseObject.code = 200;
        responseObject.message = `The Websocket server got your message.`;
        responseObject.data = {
            sentData: message.request.parameters[0]
        }

    }

    callback(socket, responseObject);
}
```

Junction-API currently has no striclty-defined opinion about exactly how you track which sockets should be written to under a given circumstance, but the above structure does create certain necessities. That is, at this point, you will most likely want to have a sort of "subscription" model, where a socket sends a request for updates regarding certain happenings, and the request results in the socket and callback being stored so that the socket can be written to at will.

---

---

## Development, Part 2

### About DataObjects

Junction-API is built on the idea that an API ought to be strongly object-oriented. What this means in practice is that a data entity you'd store in a database (like, say, a user) is completely responsible for how it's structured and behaves.

This may seem strange, but it makes quite a bit of sense in an object-oriented world. The concept of a class is that of an encapsulated entity: Something that "knows" both its own structure and how it can be used. In other API systems, these concerns are often separated. This can certainly make sense in terms of keeping individual chunks of code compact, but it also means that an object's total behavior can be spread into many nooks and crannies. This can reduce modularity, because tracking down all those places where behavior was defined easily becomes difficult. With DataObjects, both concerns of structure and behavior can be kept in one place...if that's what you want. You can stil spread things out into controllers if you like. (This is indeed the case with the core of Junction-API at the time of this writing. A great deal of logic came to reside in the controllers originally, and there's no reason to move it just now.)

Using a dataObject first involves having an appropriate database connection defined in `databaseConnections.js` or in `coreLogic/services/connections.js`. A connection, in the end, is whatever would allow a dataObject to execute commands against some sort of database system. For instance, to use MySQL, some sort of MySQL connector would need to exist to translate `dataObject.create()` to an appropriate call for a MySQL API. (With MemoryBase, an "embedded" server gets communication via `coreLogic/services/memoryBaseConnector`.)

A dataObject refers to one of these connections as `connectionName`.

DataObjects also have a defined `table`, as most database systems have the equivalent construct to keep entities grouped logically.

The `messageIdentifier` field is meant to be a "natural language-friendly" way of referring to the dataObject in output messages. It's technically optional.

The `data` object is, in many ways, the business-end of the dataObject instance. It's where the main information is stored, like - for a User dataObject - the `email` and `hashedPassword` fields.

The `dataTypesByKey` object is a very basic schema, where the `data` fields are associated with javascript types (like `number`, `string`, or `boolean`). A `data` object field can be freely set to a non-matching type, but `create` and `update` calls will fail. In many cases, this can be autogenerated from a `data` object with reasonable defaults in place, as can be seen in the `User` dataObject. You can, of course, remove the automatic setup and create the schema manually. You can also make changes after the automatic setup, which is very important for the special case of `date` objects. 

Javascript doesn't have a `date` type, and Dates are treated as `object`. As such, dataObject allows an `object` type to be essentially anything. However, if you specify `date` for a data type on a field, dataObject will try to turn that value into a new `Date` object automatically.

DataObjects can be created in more than one way. You can instantiate a dataObject by calling its constructor with no input object at all, and write fileds directly to its `data` object later. You can then call its `create` function with no input object necessary. You can initialize the dataObject by providing an input object to the constructor, and then also call the `create` function with no input object. Last, you can call the constructor with no input, and then call `create` with an input object to merge values in. Updates are similar in flexibility.

Please note that the point of the `create`, `update`, and `delete` functions is one of changing persistent data. You can do whatever you want to a dataObject without touching your database until you call one of the above.

### ReadWithQuery

For those familiar with GraphQL, this functionality has certain similarities on the surface. It's not as flexible, but it still allows you to build relatively freeform retrieval structures against dataObjects. (An important thing to note is that, in the context of an API server, a route has to be built to allow a user to POST a query object for use on a specific dataObject.)

A valid query object (in JSON) might look something like this:

```Javascript
{
	"query": {
		"filters": [
            {
			    "key": "isSuperuser",
				"operator": "==",
				"value": true,
                "convertToDate": false
			}
        ],
		"sort": {
			"key": "email",
            "direction": "descending",
            "sortAsDate": false
		},
        "fieldsToInclude": [
            "id",
            "email"
        ],
		"slice": {
			"start": 0,
			"end": 100
		}
	}
} 
```

The `filters` object defines a series of comparisons that a dataObject must satisfy (effectively, with a boolean "AND" joining each operator together). You can have any number of filter comparisons. The filter's "name" in the object is unimportant. The parts that matter are the `key`, `operator`, and `value` fields. The `key` is the data field in the dataObject that will be assessed. The `operator` is how to compare the data field with the provided `value`. It's very important to note that the value already in the database is always the left-hand operand for the `operator`. Also, if needed, `convertToDate` can be set to `true` to force the filter to treat both the provided value and the value in the record as Javascript Date objects. (Of course, if the provided value or record value can't be interpreted that way, an exception can occur.) 

Operators currently supported are ==, ===, ,>, <, >=, <=, in, !in, and `like`. The in and !in operators expect an array in the filter value. For an in operator, if the record's key matches any of the values in the array, the record is included. For !in, if the record's key matches any value in the array, the record is excluded. The `like` operator always tries to coerce the value in the database to a string, and then evaluates whether that string contains the provided `value`.

It's important to note that different database implementations will react differently to filtering against a database value that isn't a single piece of data - like an array or an object. MemoryBase, for instance, will traverse an array and compare each array element to the filter value. If anything in the array passes the filter, the filter is marked as having passed. Objects are examined as a whole. You may find that a multi-step process inside your API (or even on the client side) is necessary to achieve your expected results when filtering complex data.

The `sort` object is used after the `filters` are evaluated. The results found are ranked by the given `key`. Any `direction` value other than "descending" is considered to be ascending. Setting `sortAsDate` to true allows systems that don't necessarily enforce a Date type (like Javascript, and consequently, memoryBase) to sort by date more reliably.

The `fieldsToInclude` array allows a final query result to only include the specified attributes for each returned record. If this array is missing or empty, all attributes are returned. It's important to note that the expectation is for all other query logic to run before attributes are filtered - i.e., a query can filter or sort on a value in an attribute that is not, ultimately returned.

The `slice` object is used for paging. Internally, the filtered and sorted records are held as an array. The `start` and `end` numbers, therefore, allow for a certain "chunk" of the array to be returned.

All queries must have at least one filter, but the `sort` and `slice` objects are not required.
