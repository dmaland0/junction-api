/****************************************
*****************************************
Dependencies*/

//Builtin And Third-Party Modules

//Custom Modules
const Example = require('../dataObjects/Example').Example

/****************************************
*****************************************
Module Logic*/

exports.sayHello = function(callObject, resultCallback) {

  resultCallback(true, 200, ["aCookieForYou=chocolateChip; Path=/"], "Hello there! The URL parameter you passed, if any was: " + callObject.parameters[0], null);

};

exports.receivePost = function(callObject, resultCallback) {

  resultCallback(true, 200, ["aCookieForYou=chocolateChip; Path=/"], "Your POST request was received by the server.", callObject.payload);

};

exports.heyThere = function(callObject, resultCallback) {

  resultCallback(true, 200, ["aCookieForYou=chocolateChip; Path=/"], `Hey there ${callObject.payload.name}!`, null);

};

exports.savePost = async function(callObject, resultCallback) {

  var example = new Example();
  let message = callObject.payload.message;

  try {
    await example.create({message});
  } catch (error) {
    resultCallback(false, 500, [], "There was an error while saving your message.", error);
  }

  resultCallback(true, 200, [], "Your POST request was saved on the server.", example.data);

};
