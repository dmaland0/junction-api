/****************************************
*****************************************
Dependencies*/

//Builtin And Third-Party Modules

//Custom Modules

/****************************************
*****************************************
Module Logic*/


exports.actions = {
    example: {
        description: `Send something to the Websocket server and have it echoed back. Parameters array [0] is what you want echoed.`,
        execution: exampleFunction
    }
};

/**
 * 
 * @param {*} socket A websocket connection act on.
 * @param {*} message The incoming message from the websocket.
 * @param {*} responseObject A standardized form of response to use when resolving an action.
 * @param {callback} callback A function with parameters theSocket and theResponse.
 * 
 */
function exampleFunction (socket, message, responseObject, callback) {

    if (message.request.parameters[0]) {

        responseObject.success = true;
        responseObject.code = 200;
        responseObject.message = `The Websocket server got your message.`;
        responseObject.data = {
            sentData: message.request.parameters[0]
        }

    }

    callback(socket, responseObject);
}
