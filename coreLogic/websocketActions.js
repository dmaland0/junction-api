/****************************************
*****************************************
Dependencies*/

//Builtin And Third-Party Modules

//Custom Modules
const userActions = require("../websocketActions").actions;

/****************************************
*****************************************
Module Logic*/


let actions = {
    setPing: {
        description: `Request a server ping interval for a WebSocket connection. Parameters array [0] is the desired ping time in milliseconds.`,
        execution: require("./services/setPing").setPing
    },
    listAllActions: {
        description: `Request a list of all actions supported by the server.`,
        execution: listAllActions
    }
};

for (const action in userActions) {
    if (!actions[action]) {
        actions[action] = userActions[action];
        console.log(`\nMerged user WebsocketAction ${action} into core WebsocketActions.\n`);
    } else {
        console.log(`\nFailed to merge user WebsocketAction ${action} into core WebsocketActions, because that action name is already used in core WebsocketActions.\n`);
    }
}

/**
 * 
 * @param {*} socket A websocket connection act on.
 * @param {*} message The incoming message from the websocket.
 * @param {*} responseObject A standardized form of response to use when resolving an action.
 * @param {callback} callback A function with parameters theSocket and theResponse.
 * 
 */
exports.execute = async function(socket, message, responseObject, callback) {
    
    if (!message.request) {
        responseObject.success = false;
        responseObject.code = 400;
        responseObject.message = `The parsed message doesn't appear to contain a field named "request" that evaluates to true.`;
        responseObject.data = null;
        callback(socket, responseObject);
        return;
    }

    if (!message.request.action) {
        responseObject.success = false;
        responseObject.code = 400;
        responseObject.message = `The parsed message's action request doesn't appear to contain a field named "action" that evaluates to true.`;
        responseObject.data = null;
        callback(socket, responseObject);
        return;
    }

    if (!actions[message.request.action]) {
        responseObject.success = false;
        responseObject.code = 400;
        responseObject.message = `The parsed message's action request doesn't match with an available WebSocket action. Use the action "listAllActions" to retrieve a list of available actions on this server.`;
        responseObject.data = null;
        callback(socket, responseObject);
        return;
    }

    actions[message.request.action].execution(socket, message, responseObject, callback);
}
/**
 * @callback callback
 * @param {*} theSocket A websocket connection to respond to.
 * @param {*} theResponse A responseObject modified to be appropriate for however the action resolved.
 */

function listAllActions(socket, message, responseObject, callback) {
    let actionList = {};
    for (const action in actions) {
        if (Object.hasOwnProperty.call(actions, action)) {
            const element = actions[action];
            actionList[action] = actions[action].description;
        }
    }

    responseObject.success = true;
    responseObject.code = 200;
    responseObject.message = `A list of available actions was generated.`;
    responseObject.data = actionList;

    callback(socket, responseObject);
}
