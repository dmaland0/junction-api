/****************************************
*****************************************
Dependencies*/

//Builtin And Third-Party Modules
const fs = require("fs");
const u = require("../utilities/jsUtilities").JSUtilities;
//Custom Modules

/****************************************
*****************************************
Module Logic*/

instances = {};

class Instance {
    /**
     * Create a new instance.
     * @param {*} input
     * @param {Integer} input.writeFrequency The number of milliseconds between each on-disk backup of the database.
     * @param {String} input.fileNameBase Files related to the database will either have this name, or this name plus a unique id.
     * @param {String} input.filePath The directory where on-disk backups/ archives should reside.
     */
    constructor(input = {}) {
        this.writeFrequency = 30000;
        this.fileNameBase = "memoryBase";
        this.filePath = "./coreLogic/memoryBase/files/";

        //There's no need to set these - they're used internally.
        this.currentFile = null;
        this.currentFileCounter = 0;
        this.idCounter = 0;
        this.data = {};
        this.modified = false;

        u.forIn(input, (key, value) => {
            this[key] = value;
        });

        fs.readdir(this.filePath, (error, files) => {
            if (error) {
                console.warn(`\nWARNING: MemoryBase tried to read ${this.filePath}, but an error occurred: ${error}\nMemoryBase can't create directories for itself, only files. Please make sure the directory exists.`)
            } else {
                let latestFile = this.fileNameBase;

                u.forIn(files, (index, file) => {
                    if (file.indexOf(this.fileNameBase) >= 0) {
                        latestFile = file;
                        this.currentFileCounter += 1;
                    }
                });

                this.currentFile = `${this.filePath}${latestFile}`;

                let now = new Date();

                fs.readFile(this.currentFile, (error, data) => {
                    if (error) {
                        console.warn(`\nWARNING: MemoryBase tried to read ${this.currentFile}, but an error occurred: ${error}.\nFinding no file at all is normal when instantiating a MemoryBase that has never been used before, but abnormal otherwise.\n`);
                    } else {
                        try {
                            this.data = JSON.parse(data);
                            let keys = Object.keys(this.data);
                            let later = new Date();
                            console.log(`"${this.fileNameBase}" loaded existing data as the following tables: ${keys}. Elapsed time: ${(later-now)/1000} seconds.`)
                        } catch (error) {
                            console.warn(`\nWARNING: MemoryBase tried to read ${this.currentFile}, but an error occurred: ${error}.\nThis can happen when MemoryBase encounters an empty file, or a corrupted one.`);
                        }
                        
                    }
                });

            }

        });

        this.writeInterval = setInterval(() => {
            if (this.currentFile && this.modified) {
                this.modified = false;
                let writeData = JSON.stringify(this.data, null, '\t');
                let now = new Date();

                fs.writeFile(this.currentFile, writeData, (error) => {
                    if (error) {
                        console.log(`\nWARNING: A MemoryBase instance tried to write ${this.currentFile}, but an error occurred: ${error}\n`);
                        this.modified = true;
                    } else {
                        let later = new Date();
                        console.log(`A MemoryBase instance wrote ${this.currentFile} at ${new Date()}. Elapsed time: ${(later - now)/1000} seconds.`)
                    }
                });
            }
        }, this.writeFrequency);

    }

    /**
     * MemoryBase does not support pre-defined tables, so calling this function throws an error. (The function is included for the sake of future consistency.)
     */
    buildTable() {
        throw (`WARNING: MemoryBase does not support pre-defining tables. Data consistency is up to the API developer.`)
    }

    /**
     * Create a new record.
     * @param {*} dataObject A dataObject to use for the operation.
     * @param {String} dataObject.table The table name where the record should reside.
     * @param {String} dataObject.data The information to be used for constructing the record.
     * @returns The input dataObject.
     * @throws An error if the operation fails.
     */
    async create(dataObject) {
        let table = this.data[dataObject.table];

        if (!table) {
            this.data[dataObject.table] = {};
            table = this.data[dataObject.table];
        }

        let next = 0;

        u.forIn(table, (id) => {
            next = id;
        });

        next = parseInt(next)
        next += 1;

        try {
            dataObject.data.id = next;
            table[next] = dataObject.data;
            this.modified = true;
            this.idCounter += 1;
            return dataObject;
        } catch (error) {
            throw (`Create encountered an error: ${error}`);
        }

    }

    /**
     * Find a record based on its unique identifier.
     * @param {*} dataObject A dataObject to use for the operation.
     * @param {*} dataObject.data.id The unique identifier to be used to find the record.
     * @param {String} dataObject.table The table to be searched.
     * @returns An array of results, which may be empty.
     * @throws An error if the requested table doesn't exist.
     */
    async readOne(dataObject) {
        let result = {};
        let id = dataObject.data.id;
        let table = dataObject.table;

        if (!this.data[table]) {
            throw (`The table ${table} does not exist in this MemoryBase. This can be because no records of the type requested have been created yet.`);
        }

        if (this.data[table][id]) {
            result = this.data[table][id];
        }

        return result;
    }

    /**
     * Find the last record in a table.
     * @param {*} dataObject A dataObject to use for the operation.
     * @param {*} dataObject.data.id The unique identifier to be used to find the record.
     * @param {String} dataObject.table The table to be searched.
     * @returns An array of results, which may be empty.
     * @throws An error if the requested table doesn't exist.
     */
    async readLast(dataObject) {
        let result = {};
        let table = dataObject.table;

        if (!this.data[table]) {
            throw (`The table ${table} does not exist in this MemoryBase. This can be because no records of the type requested have been created yet.`);
        }

        let ids = Object.keys(this.data[table]);
        let lastID = ids[ids.length - 1]
        result = this.data[table][lastID];
        return result;
    }

    /**
     * Read several records from a database table.
     * @param {*} dataObject A dataObject to use for the operation.
     * @param {Integer} dataObject.start The ID of the first record to be retrieved.
     * @param {String} dataObject.table The table to retrieve records from.
     * @param {Integer} dataObject.limit The maximum number of records to retrieve.
     * @returns An array of results, which may be empty.
     * @throws An error if the requested table doesn't exist.
     */
    async readMany(dataObject) {
        let results = [];
        let start = dataObject.start;
        let table = dataObject.table;

        if (!this.data[table]) {
            throw (`The table ${table} does not exist in this MemoryBase. This can be because no records of the type requested have been created yet.`);
        }

        let indices = Object.keys(this.data[table]);
        let tableData = this.data[table];

        indices.map((index) => {
            if (results.length < dataObject.limit) {
                if (tableData[index].id >= dataObject.start) {
                    results.push(tableData[index]);
                }
            }
        });

        return results;

    }

    /**
     * Find the first record where the specified field matches the specified value.
     * @param {*} dataObject A dataObject to use for the operation.
     * @param {*} dataObject.key The field name to use when matching criteria.
     * @param {*} dataObject.value The value that the field must hold to match successfully.
     * @param {String} dataObject.table The table to retrieve records from.
     * @returns An array of results, which may be empty.
     * @throws An error if the requested table doesn't exist.
     */
    async readFirstWithKeyValue(dataObject) {
        let result = {};
        let key = dataObject.key;
        let value = dataObject.value;
        let found = false;
        let table = dataObject.table;

        if (!this.data[table]) {
            throw (`The table ${table} does not exist in this MemoryBase. This can be because no records of the type requested have been created yet.`);
        }

        let ids = Object.keys(this.data[table])

        for (let index = 0; index < ids.length; index++) {
            let id = ids[index];
            let record = this.data[table][id];

            if (record[key]) {
                if (record[key] === value) {
                    result = record;
                    break
                }
            }
        }

        return result;

    }

    /**
     * Find all records where the specified field matches the specified value.
     * @param {*} dataObject A dataObject to use for the operation.
     * @param {*} dataObject.key The field name to use when matching criteria.
     * @param {*} dataObject.value The value that the field must hold to match successfully.
     * @param {String} dataObject.table The table to retrieve records from.
     * @returns An array of results, which may be empty.
     * @throws An error if the requested table doesn't exist.
     */
    async readAllWithKeyValue(dataObject) {
        let results = [];
        let key = dataObject.key;
        let value = dataObject.value;
        let table = dataObject.table;

        if (!this.data[table]) {
            throw (`The table ${table} does not exist in this MemoryBase. This can be because no records of the type requested have been created yet.`);
        }

        let ids = Object.keys(this.data[table])

        for (let index = 0; index < ids.length; index++) {
            let id = ids[index];
            let record = this.data[table][id];

            if (record[key]) {
                if (record[key] === value) {
                    results.push(record);
                }
            }
        }

        return results;

    }

    /**
     * Retrieve all the records from a given table.
     * @param {*} dataObject A dataObject to use for the operation.
     * @param {String} dataObject.table The table to retrieve records from.
     * @returns An array of results, which may be empty.
     * @throws An error if the requested table doesn't exist.
     */
    async readAll(dataObject) {
        let results = [];
        let table = dataObject.table;

        if (!this.data[table]) {
            throw (`The table ${table} does not exist in this MemoryBase. This can be because no records of the type requested have been created yet.`);
        }

        let ids = Object.keys(this.data[table]);

        ids.map((id) => {
            results.push(this.data[table][id]);
        });
        
        return results;

    }

    /**
     * Retrieve records from a given table that fit a query description.
     * @param {*} dataObject A dataObject to use for the operation.
     * @param {String} dataObject.table The table to retrieve records from.
     * @returns An array of results, which may be empty.
     * @throws An error if the requested table doesn't exist.
     */
    async readWithQuery(dataObject) {
        let results = [];
        let table = dataObject.table;

        if (!this.data[table]) {
            throw (`The table ${table} does not exist in this MemoryBase. This can be because no records of the type requested have been created yet.`);
        }

        let ids = Object.keys(this.data[table]);
        let query = dataObject.query;

        if (!query) {
            throw(`ReadWithQuery can't be called with an empty query.`);
        }

        try {

            for (const id of ids) {
                let record = this.data[table][id]

                if (query.filters) {
                    let filters = query.filters;
                    let filterPass = true;

                    for (const filter of filters) {
                        let recordValue = record[filter.key];
                        let filterValue = filter.value;
                        let transformedRecordValue = null;

                        if (queryOperators[filter.operator]) {
                            if (filterPass) {

                                /**
                                 * If the query sends a Date, we can into problems when a Date is stored as
                                 * a string. This block tries to coerce the record value to a Date when one
                                 * is encountered as a filter value, or if the filter is explicitly
                                 * instructed to consider the value as a date.
                                 */
                                try {
                                    if (filterValue instanceof(Date) || filter.convertToDate == true) {
                                        transformedRecordValue = new Date(recordValue);
                                        filterValue = new Date(filterValue);
                                    } else {
                                        transformedRecordValue = recordValue;
                                    }
                                } catch (error) {
                                    console.error(`A readWithQuery operation attempted to transform a record value into a Date object, but failed: ${error}`);
                                    transformedRecordValue = recordValue;
                                }

                                //Start this process by assuming that the filter has not passed.
                                filterPass = false;

                                //For arrays, traverse the values and set filterPass = true if anything passes.
                                if (transformedRecordValue.forEach) {
                                    transformedRecordValue.forEach(function(value, index) {
                                        if (queryOperators[filter.operator](value, filterValue)) {
                                            filterPass = true;
                                        }
                                    })
                                } else {
                                    if (queryOperators[filter.operator](transformedRecordValue, filterValue)) {
                                        filterPass = true;
                                    }
                                }

                            }
                        } else {
                            throw `No matching operator found for ${filter.operator}. Available filter operators are ${Object.keys(queryOperators)}`;
                        }
                    }

                    if (filterPass) {
                        results.push(record);
                    }
                } else {
                    throw `Queries must have a filters array.`
                }
            }

            if (query.sort && query.sort.key) {
                results.sort((a,b) => {
                    let key = query.sort.key;
                    let invert = false;
                    let returnValue = 0;

                    let sortA = null;
                    let sortB = null;

                    if (query.sort.sortAsDate) {
                        try {
                            sortA = new Date(a[key]);
                            sortB = new Date(b[key]);
                        } catch (error) {
                            console.error(`A readWithQuery sort operation attempted to transform a record value into a Date object, but failed: ${error}`);
                            sortA = a[key];
                            sortB = b[key];
                        }
                    } else {
                        sortA = a[key];
                        sortB = b[key];
                    }

                    if (query.sort.direction === "descending") {
                        invert = true;
                    }

                    if (sortA < sortB) {
                        returnValue = -1;
                    }

                    if (sortA > sortB) {
                        returnValue = 1;
                    }

                    if (invert) {
                        returnValue *= -1;
                    }

                    return returnValue;
                });
            }

            if (query.slice && query.slice.start && query.slice.end) {
                results = results.slice(query.slice.start, query.slice.end);
            }

            if (query.fieldsToInclude && query.fieldsToInclude.length > 0) {
                let projectedResults = [];

                results.map((result) => {
                    let projectedObject = {};

                        query.fieldsToInclude.map((field) => {
                            projectedObject[field] = result[field];
                        })

                        projectedResults.push(projectedObject);
                })

                results = projectedResults;
            }

            return results;
            
        } catch (error) {
            throw `ReadWithQuery encountered an error: ${error}`
        }
    }

    /**
     * Retrieve recent records.
     * @param {*} dataObject A dataObject to use for the operation.
     * @param {String} dataObject.table The table to retrieve records from.
     * @param {Number} dataObject.limit The maximum number of results to retrieve.
     * @returns An array of results, which may be empty. The array is sorted such that the most recent record is first.
     * @throws An error if the requested table doesn't exist.
     */
    async readMostRecent(dataObject) {
        let results = [];
        let table = dataObject.table;
        let limit = dataObject.limit;

        if (!this.data[table]) {
            throw (`The table ${table} does not exist in this MemoryBase. This can be because no records of the type requested have been created yet.`);
        }

        let ids = Object.keys(this.data[table])

        ids.map((id) => {
            results.push(this.data[table][id]);

            results.sort((a,b) => {

                if (a.createdAt && b.createdAt) {

                    let aDate = null;
                    let bDate = null;

                    try {
                        aDate = new Date(a.createdAt);
                        bDate = new Date(b.createdAt);
                    } catch (error) {
                        console.error(`A readMostRecent operation attempted to transform a record's createdAt value into a Date object, but failed: ${error}`);
                        aDate = a.createdAt;
                        bDate = b.createdAt;
                    }

                    if (aDate > bDate) {
                        return -1
                    }

                    if (aDate < bDate) {
                        return 1
                    }

                    return 0
                } else {
                    if (a.id && b.id) {

                        if (a.id > b.id) {
                            return -1
                        } 

                        if (a.id < b.id) {
                            return 1
                        }

                        return 0
                    } else {
                        throw (`MemoryBase could not infer a most-recent sort order, because the records in question don't have createdAt fields, and at least one record is missing an id field.`)
                    }

                }
            })

            if (results.length > limit) {
                //The most recent result will always be first, so if we hit a limit we should remove the last element of the array.
                results.pop();
            }
        })

        return results;
    }

    /**
     * Retrieve records where any field contains the search query.
     * @param {*} dataObject A dataObject to use for the operation.
     * @param {String} dataObject.table The table to retrieve records from.
     * @param {String} dataObject.value The value to search for.
     * @returns An array of results, which may be empty.
     * @throws An error if the requested table doesn't exist.
     */
     async readAllWithAnyKeyHavingValue(dataObject) {
        let results = [];
        let table = dataObject.table;
        let value = dataObject.value;

        if (!this.data[table]) {
            throw (`The table ${table} does not exist in this MemoryBase. This can be because no records of the type requested have been created yet.`);
        }

        let ids = Object.keys(this.data[table])

        for (let index = 0; index < ids.length; index++) {
            let id = ids[index];
            let record = this.data[table][id];
            let found = false;

            Object.keys(record).map((key) => {
                if (!found) {
                    if (record[key].toString().indexOf(value) > -1) {
                        found = true;
                        results.push(JSON.parse(JSON.stringify(record)));
                    }
                }
            });

        }

        return results;
    }

    /**
     * Modify a record.
     * @param {*} dataObject A dataObject to use for the operation.
     * @param {String} dataObject.table The table where the record resides.
     * @param {*} dataObject.data.id The unique identifier of the record to be modified.
     * @returns The modified dataObject.
     * @throws An error if the requested table or record doesn't exist.
     */
    async update(dataObject) {
        let table = this.data[dataObject.table];

        if (!table) {
            throw (`The table ${table} does not exist, so a record can't be updated. This can be because no records of the type requested have been created yet.`);
        }

        if (!table[dataObject.data.id]) {
            throw (`No record with id ${dataObject.data.id} was found in ${table}`);
        }

        table[dataObject.data.id] = dataObject.data;
        this.modified = true;

        return dataObject;

    }

    /**
     * Remove a record.
     * @param {*} dataObject A dataObject to use for the operation.
     * @param {String} dataObject.table The table where the record resides.
     * @param {*} dataObject.data.id The unique identifier of the record to be modified.
     * @returns The dataObject input to the operation.
     * @throws An error if the requested table or record doesn't exist.
     */
    async delete(dataObject) {
        let table = this.data[dataObject.table];

        if (!table) {
            throw (`The table ${table} does not exist in this MemoryBase. This can be because no records of the type requested have been created yet.`, null);
        }

        if (!table[dataObject.data.id]) {
            throw (`No record with id ${dataObject.data.id} was found in ${table}`);
        }

        delete table[dataObject.data.id]
        this.modified = true;

        return dataObject

    }

}

exports.requests = {

    handshake: async function(input) {
        output = {message: "", data: null, error: null};
        if (input.handshake != "handshake") {
            output.message = "You can call the initial handshake function without an object where the field handshake is true, but it's cooler if you do."
        } else {
            output.message = "MemoryBase gives you a friendly, vigorous handshake."
        }
        return output;
    },

    initialize: async function (input) {
        output = {message: "", data: null, error: null};

        if (!input.fileNameBase) {
            output.error = "Initialize can't be called without the fileNameBase fields";
            return output;
        }

        if (instances[input.connectionName]) {
            output.error = `An instance of "${input.connectionName}" already exists. No action taken.`;
            return output;
        }

        try {
            instances[input.connectionName] = new Instance({writeFrequency: input.writeFrequency, fileNameBase: input.fileNameBase});
            output.message = `\n\n**********\nMemoryBase instance "${input.connectionName}" created.\nInstances can be created but still have non-fatal file errors.\n**********\n\n`;
            return output;
        } catch (error) {
            output.error = error;
            return output;
        }
        
    },

    create: async function (input) {
        output = {message: "", data: null, error: null};

        try {
            let creation = await instances[input.connectionName].create(input);
            output.data = JSON.parse(JSON.stringify(creation));
            return output;
        } catch (error) {
            output.error = error;
            return output;
        }
        
    },

    readOne: async function (input) {
        output = {message: "", data: null, error: null};

        try {
            let read = await instances[input.connectionName].readOne(input);
            output.data = JSON.parse(JSON.stringify(read));
            return output;
        } catch (error) {
            output.error = error;
            return output;
        }
        
    },

    readAll: async function (input) {
        output = {message: "", data: null, error: null};

        try {
            let read = await instances[input.connectionName].readAll(input);
            output.data = JSON.parse(JSON.stringify(read));
            return output;
        } catch (error) {
            output.error = error;
            return output;
        }
        
    },

    readAllWithKeyValue: async function (input) {
        output = {message: "", data: null, error: null};

        try {
            let read = await instances[input.connectionName].readAllWithKeyValue(input);
            output.data = JSON.parse(JSON.stringify(read));
            return output;
        } catch (error) {
            output.error = error;
            return output;
        }
        
    },

    readFirstWithKeyValue: async function (input) {
        output = {message: "", data: null, error: null};

        try {
            let read = await instances[input.connectionName].readFirstWithKeyValue(input);
            output.data = JSON.parse(JSON.stringify(read));
            return output;
        } catch (error) {
            output.error = error;
            return output;
        }
        
    },

    readLast: async function (input) {
        output = {message: "", data: null, error: null};

        try {
            let read = await instances[input.connectionName].readLast(input);
            output.data = JSON.parse(JSON.stringify(read));
            return output;
        } catch (error) {
            output.error = error;
            return output;
        }
        
    },

    readMany: async function (input) {
        output = {message: "", data: null, error: null};

        try {
            let read = await instances[input.connectionName].readMany(input);
            output.data = JSON.parse(JSON.stringify(read));
            return output;
        } catch (error) {
            output.error = error;
            return output;
        }
        
    },

    readWithQuery: async function (input) {
        output = {message: "", data: null, error: null};

        try {
            let read = await instances[input.connectionName].readWithQuery(input);
            output.data = JSON.parse(JSON.stringify(read));
            return output;
        } catch (error) {
            output.error = error;
            return output;
        }
        
    },

    readMostRecent: async function (input) {
        output = {message: "", data: null, error: null};

        try {
            let read = await instances[input.connectionName].readMostRecent(input);
            output.data = JSON.parse(JSON.stringify(read));
            return output;
        } catch (error) {
            output.error = error;
            return output;
        }
    },

    readAllWithAnyKeyHavingValue: async function(input) {
        output = {message: "", data: null, error: null};

        try {
            let read = await instances[input.connectionName].readAllWithAnyKeyHavingValue(input);
            output.data = JSON.parse(JSON.stringify(read));
            return output;
        } catch (error) {
            output.error = error;
            return output;
        }
    },

    update: async function (input) {
        output = {message: "", data: null, error: null};

        try {
            let updated = await instances[input.connectionName].update(input);
            output.data = JSON.parse(JSON.stringify(updated));
            return output;
        } catch (error) {
            output.error = error;
            return output;
        }
        
    },

    delete: async function (input) {
        output = {message: "", data: null, error: null};

        try {
            let deleted = await instances[input.connectionName].delete(input);
            output.data = JSON.parse(JSON.stringify(deleted));
            return output;
        } catch (error) {
            output.error = error;
            return output;
        }
        
    }
};

queryOperators = {
    "in": function(valueInRecord, valuesInFilter) {
        for (const value of valuesInFilter) {
            if (valueInRecord == value) {
                return true;
            }
        }
        return false;
    },

    "!in": function(valueInRecord, valuesInFilter) {
        for (const value of valuesInFilter) {
            if (valueInRecord == value) {
                return false;
            }
        }
        return true;
    },

    "like": function(valueInRecord,valueInFilter) {
        if (valueInRecord.toString().indexOf(valueInFilter) > -1) {
            return true;
        } else {
            return false;
        }
    },

    "==": function(valueInRecord,valueInFilter) {
        if (valueInRecord == valueInFilter) {
            return true;
        } else {
            return false;
        }
    },

    "!=": function(valueInRecord,valueInFilter) {
        if (valueInRecord != valueInFilter) {
            return true;
        } else {
            return false;
        }
    },

    "===": function(valueInRecord,valueInFilter) {
        if (valueInRecord === valueInFilter) {
            return true;
        } else {
            return false;
        }
    },

    "!==": function(valueInRecord,valueInFilter) {
        if (valueInRecord !== valueInFilter) {
            return true;
        } else {
            return false;
        }
    },

    ">": function(valueInRecord,valueInFilter) {
        if (valueInRecord > valueInFilter) {
            return true;
        } else {
            return false;
        }
    },
    
    "<": function(valueInRecord,valueInFilter) {
        if (valueInRecord < valueInFilter) {
            return true;
        } else {
            return false;
        }
    },

    ">=": function(valueInRecord,valueInFilter) {
        if (valueInRecord >= valueInFilter) {
            return true;
        } else {
            return false;
        }
    },
    
    "<=": function(valueInRecord,valueInFilter) {
        if (valueInRecord <= valueInFilter) {
            return true;
        } else {
            return false;
        }
    }
};
