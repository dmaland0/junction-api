/****************************************
*****************************************
Dependencies*/

//Builtin And Third-Party Modules
const u = require("../utilities/jsUtilities").JSUtilities;
//Custom Modules
const DataObject = require("../services/dataObject").DataObject;
/****************************************
*****************************************
Module Logic*/

class FailedLogin extends DataObject {

    /**
     * Create a new Failed Login entry.
     * @param {*} input Properties to set the data fields with.
     * @param {Integer} input.id The desired ID.
     * @param {String} input.ip The host used for the login attempt.
     * @param {String} input.email The email used in the login attempt.
     * @param {String} input.time The time of the login attempt.
     */
    constructor(input = {}) {

        super({
            connectionName: "core",
            table: "failedLogins",
            messageIdentifier: "Failed Logins",
            data: {
                id: 0,
                ip: "",
                email: "",
                time: new Date()
            },
            dataTypesByKey: {
            }
        });

        //If there are reasonable default values set in the data object, the dataTypesByKey object can be set automatically.
        let keys = Object.keys(this.data);
        keys.map((key) => {
            this.dataTypesByKey[key] = typeof(this.data[key])
        });

        u.forIn(input, (key, value) => {
            this.data[key] = value;
        });

    }

}

exports.FailedLogin = FailedLogin;
