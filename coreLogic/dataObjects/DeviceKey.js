/****************************************
*****************************************
Dependencies*/

//Builtin And Third-Party Modules
const u = require("../utilities/jsUtilities").JSUtilities;
//Custom Modules
const DataObject = require("../services/dataObject").DataObject;
/****************************************
*****************************************
Module Logic*/

class DeviceKey extends DataObject {

    /**
     * Create a new Device Key.
     * @param {*} input Properties to set the data fields with.
     * @param {Integer} input.id The desired ID.
     * @param {String} input.userID The user associated with the key.
     * @param {String} input.authorized If the device with the key is authorized or not.
     * @param {String} input.key The key itself.
     * @param {String} input.deviceInfo Miscellaneous information the device has made available about itself.
     * @param {String} input.authorizationCode The necessary code to activate the key.
     */
    constructor(input = {}) {

        super({
            connectionName: "core",
            table: "deviceKeys",
            messageIdentifier: "Device Key",
            data: {
                id: 0,
                userID: 0,
                authorized: false,
                authorizationCode: "",
                key: "",
                deviceInfo: ""
            },
            dataTypesByKey: {
            }
        });

        let keys = Object.keys(this.data);
        keys.map((key) => {
            this.dataTypesByKey[key] = typeof(this.data[key])
        });

        u.forIn(input, (key, value) => {
            this.data[key] = value;
        });

    }

}

exports.DeviceKey = DeviceKey;
