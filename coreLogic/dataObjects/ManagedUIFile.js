/****************************************
*****************************************
Dependencies*/

//Builtin And Third-Party Modules
const u = require("../utilities/jsUtilities").JSUtilities;
//Custom Modules
const DataObject = require("../services/dataObject").DataObject;
/****************************************
*****************************************
Module Logic*/

class ManagedUIFile extends DataObject {

    /**
     * Create a new managed UI file.
     * @param {*} input Properties to set the data fields with.
     * @param {Integer} input.id The desired ID.
     * @param {String} input.fileName A human-readable identifier for the file.
     * @param {String} input.content The file body, expected to be HTML.
     * @param {String} input.published Whether or not the file is available.
     */
    constructor(input = {}) {

        super({
            connectionName: "core",
            table: "managedUIFiles",
            messageIdentifier: "Managed UI Files",
            data: {
                id: 0,
                fileName: "",
                content: "",
                published: false
            },
            dataTypesByKey: {
                id: "number",
                fileName: "string",
                content: "string",
                published: "boolean"
            }
        });

        u.forIn(input, (key, value) => {
            this.data[key] = value;
        });

    }

}

exports.ManagedUIFile = ManagedUIFile;
