/****************************************
*****************************************
Dependencies*/

//Builtin And Third-Party Modules

//Custom Modules
const DataObject = require("../services/dataObject").DataObject;

/****************************************
*****************************************
Module Logic*/

class RegistrationKey extends DataObject {

    /**
     * Create a new Registration Key.
     * @param {*} input Properties to set the data fields with.
     * @param {Integer} input.id The desired ID.
     * @param {String} input.key A key value that should be hard to guess.
     * @param {String} input.userID The user that this key corresponds to.
     */
    constructor(input = {}) {

        super({
            connectionName: "core",
            table: "registrationKeys",
            messageIdentifier: "Example",
            data: {
                id: 0,
                key: "",
                userID: 0
            },
            dataTypesByKey: {
            }
        });

        let keys = Object.keys(this.data);

        keys.map((key) => {
            this.dataTypesByKey[key] = typeof(this.data[key])
        });

        let inputKeys = Object.keys(input)

        inputKeys.map((inputKey) => {
            this.data[inputKey] = input[inputKey];
        });

    }

}

exports.RegistrationKey = RegistrationKey;
