/****************************************
*****************************************
Dependencies*/

//Builtin And Third-Party Modules
const u = require("../utilities/jsUtilities").JSUtilities;
//Custom Modules
const DataObject = require("../services/dataObject").DataObject;
/****************************************
*****************************************
Module Logic*/

class PasswordResetKey extends DataObject {

    /**
     * Create a new Password Reset Key.
     * @param {*} input Properties to set the data fields with.
     * @param {Integer} input.id The desired ID.
     * @param {String} input.userID The user associated with the key.
     * @param {String} input.key The key itself.
     */
    constructor(input = {}) {

        super({
            connectionName: "core",
            table: "resetKeys",
            messageIdentifier: "Reset Key",
            data: {
                id: 0,
                userID: 0,
                key: "",
            },
            dataTypesByKey: {
            }
        });

        let keys = Object.keys(this.data);

        keys.map((key) => {
            this.dataTypesByKey[key] = typeof(this.data[key])
        });

        let inputKeys = Object.keys(input)

        inputKeys.map((inputKey) => {
            this.data[inputKey] = input[inputKey];
        });

    }

}

exports.PasswordResetKey = PasswordResetKey;
