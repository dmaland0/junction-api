/****************************************
*****************************************
Dependencies*/

//Builtin And Third-Party Modules
const u = require("../utilities/jsUtilities").JSUtilities;
//Custom Modules
const DataObject = require("../services/dataObject").DataObject;
/****************************************
*****************************************
Module Logic*/

class User extends DataObject {

    /**
     * Create a new User.
     * @param {*} input Properties to set the data fields with.
     * @param {Integer} input.id The desired ID.
     * @param {String} input.email An email address.
     * @param {String} input.isSuperuser If this user has admin privileges or not.
     * @param {String} input.salt The modifier added to this user's password.
     * @param {String} input.accessToken The user's current token.
     * @param {String} input.tokenExpires When the current access token can no longer be used.
     * @param {String} input.lastLoginIP Where the user last logged in from.
     * @param {String} input.lastLoginTime When the user last logged in.
     * @param {String} input.hashedPassword The one-way hash of the user's password and salt.
     * @param {String} input.keyFileHash The one-way hash of the user's key file.
     * @param {String} input.mustChangePassword Whether or not a user must change their password before being authenticated.
     * @param {String} input.createdAt When the user was created.
     */
    constructor(input = {}) {

        super({
            connectionName: "core",
            table: "users",
            messageIdentifier: "Users",
            data: {
                id: 0,
                email: "",
                isSuperuser: false,
                salt: "",
                accessToken: "",
                tokenExpires: new Date(0),
                lastLoginIP: "",
                lastLoginTime: new Date(0),
                hashedPassword: "",
                mustChangePassword: false,
                createdAt: new Date(),
            },
            dataTypesByKey: {
            },
            secretFields: ["salt", "accessToken", "hashedPassword"]
        });

        let keys = Object.keys(this.data);
        keys.map((key) => {
            this.dataTypesByKey[key] = typeof(this.data[key])
        });

        this.dataTypesByKey.tokenExpires = "date";
        this.dataTypesByKey.lastLoginTime = "date";
        this.dataTypesByKey.createdAt = "date";

        u.forIn(input, (key, value) => {
            this.data[key] = value;
        });

    }

}

exports.User = User;
