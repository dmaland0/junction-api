/****************************************
*****************************************
Dependencies*/

//Builtin And Third-Party Modules
const userJITActions = require("../justInTimeRenderingActions").justInTimeRenderingActions;
const u = require("./utilities/jsUtilities").JSUtilities;
//Custom Modules

/****************************************
*****************************************
Module Logic*/

//All function calls should be uniform in their parameters, because fileController doesn't know ahead of time
//what's being called. It's theoretically possible to get around this, but it's probably easier not to.

function now(parameterObject, callback) {
  var now =  new Date().toString();
  callback(now);
}

function requestHost(parameterObject, callback) {

  var host = null;

  if (parameterObject.headers !== undefined) {
    host = parameterObject.headers.host;
  } else {
    host = "parameterObject.headers undefined."
  }

  callback(host);

}

//JustInTimeRenderingActions are functions defined above.
//Each function must implement a callback which takes the value intended for JIT rendering as a parameter.
exports.justInTimeRenderingActions = {

  ServerTime: now,
  RequestHost: requestHost

}

//Merge any user defined JIT rendering actions with the core.
u.forIn(userJITActions, function(index, action) {
  exports.justInTimeRenderingActions[index] = action;
});
