/****************************************
*****************************************
Dependencies*/

//Builtin And Third-Party Modules
const fs = require("fs");
const u = require("./utilities/jsUtilities").JSUtilities;
//Custom Modules
const environmentVariables = require("../environmentVariables").environmentVariables;
const justInTimeRenderingActions = require("./coreJustInTimeRenderingActions").justInTimeRenderingActions;
const ManagedUIFile = require("./dataObjects/ManagedUIFile").ManagedUIFile;
const usersController = require("./controllers/usersController");
const LogEntry = require("./services/logger").LogEntry;

/****************************************
*****************************************
Module Logic*/

/*
handleJITRendering

content: string, a file's internals

headers: object, the request headers from the server call

callback: function, a callback
  parameter 1: string, a file's internals after JIT rendering.

*/

function handleJITRendering(request, content, headers, callback) {

  var pattern = /<JITRender([^<>]+)>/g;
  var patternMatches = [];

  //Recursive pattern matching for JITRender calls.
  function pushMatch () {
    var match = pattern.exec(content);

    if (match) {

      //If there's a match, we should build matchObjects.
      var matchObject = {tag: match[0], action: match[1]};

      if (!u.valueIsIn(matchObject, patternMatches).found) {
        //Only push a new match if the match doesn't exist already.
        patternMatches.push({tag: match[0], action: match[1]});
      }

      //If there was one match, there might be more, so make a recursive call.
      pushMatch();

    }

  }

  function runMatch () {

    if (index < patternMatches.length) {

      var thisMatch = patternMatches[index];
      var identifier = thisMatch.tag;

      if (justInTimeRenderingActions[thisMatch.action]) {
        //If valid action, execute...

        justInTimeRenderingActions[thisMatch.action]({headers, request}, function(actionResult) {

          content = content.replace(identifier, actionResult);
          index += 1;
          runMatch();

        });

      } else {
        //Invalid actions generate a warning.
        var tagPattern = /[<>]/g
        content = content.replace(identifier, "No justInTimeRenderingActions function found for: " + identifier.replace(tagPattern, ""));
        index += 1;
        runMatch();

      }

    } else {
      //If we've run out of matches, flush patternMatches and re-run pushMatch to catch any JITRender tags embedded in JITRender content.
      patternMatches = [];
      pushMatch();

      if (patternMatches.length > 0) {

        //If something new was found, run recursively.
        index = 0;
        runMatch();

      } else {
        //If there's no more to do, run the callback with the content included.
        callback(content);
      }

    }

  }

  //"Bootstrap" pushMatch.
  pushMatch();

  //"Bootstrap" runMatch.
  var index = 0;
  runMatch();

}

/*
handleIncludes

path: string, the original path for the file request

content: string, a file's internals

headers: object, the request headers from the server call

callback: function, a callback
  parameter 1: string, a file's internals after includes have been processed.

*/

function handleIncludes(request, path, content, headers, callback) {

  //Look for <Include([anything])>
  var pattern = /<Include\(([^<]+)\)>/g;
  var searchResult = [];

  function search () {

    searchResult = pattern.exec(content);

    if (searchResult) {

      //If the search finds something, try to get the requested file.
      var identifier = searchResult[0];
      var includePath = searchResult[1];

      exports.getFile(request, includePath, headers, null, function(success, fullPath, contentObject) {

        if (success) {

          //If the file is found, replace the Include tag with the file's contents.
          content = content.replace(identifier, contentObject.data);
          //Continue with a recursive call.
          search();

        } else {

          //If nothing can be found, replace the Include tag with a warning.

          content = content.replace(identifier, "Could not find a text-based file to include based on the requested path: " + includePath + ". Additional information/ actual path used by server: " + fullPath);
          //Continue with a recursive call.
          search();

        }

      })

    } else {
      //If the search result is falsey, there are no more matches and the callback can be run.
      callback(content);
    }

  }

  //Bootstrap search.
  search();

}

/*
handleFileContent

path: string, the requested file's location

content: string, the file's internals

headers: object, the request headers from the server call

routerCallback: function, a callback
  parameter 1: boolean, whether the file could be read
  parameter 2: object
    type: string, an indication of content-type
    data: string, the file contents

*/

function handleFileContent(request, path, content, headers, routerCallback) {

  function continueHandle(request) {

    handleJITRendering(request, content, headers, function(newContent) {

      handleIncludes(request, path, newContent, headers, function(finalContent) {
        //When handleIncludes is complete, call the router with the type and file.
        routerCallback(true, null, {"type": type, "data": finalContent});
      });

    });

  }

  //Try to guess common content types.
  var type = "";
  var extension = path.substring(path.lastIndexOf(".")+1);

  u.forIn(environmentVariables.mimeTypes, function(index, mimeType) {

    if (mimeType[0] == extension) {
      type = mimeType[1];
    }

  });

  if (type.search("base64") < 0) {

    //We should only do JIT and Include rendering for text files.

    content = content.toString("utf8");

    var pattern = /<RequiresLogin>/g
    var match = pattern.exec(content);

    var allCookies = null;

    if (match) {
      //If the <RequiresLogin> tag is present, try to authenticate the user.
      if (headers.cookie) {
        allCookies = headers.cookie.split("; ");
      } else {
        allCookies = [];
      }

      var callObject = {};
      callObject.cookies = allCookies;

      //We need to set these empties, or else authenticateWithToken will throw an exception and crash.
      callObject.request = {};
      callObject.request.headers = {};

      usersController.authenticateWithToken(callObject, function(success, code, cookies, message, user) {

        if (!success) {
          //If the user couldn't be authenicated, send back a message.
          routerCallback(true, null, {"type":"text/plain", "data": "You must be logged in to access the file requested, but the token authentication failed: " + message});
        } else {
          //Don't pass the RequiresLogin tag to the client.
          content = content.replace(pattern, "");
          continueHandle(request);
        }

        return user;

      });

    } else {
      //No match means that we can continue.
      continueHandle(request);
    }

  } else {

    //Call back immediately with success and data if the file is binary.
    routerCallback(true, null, {"type": type, "data": content});

  }

}

/*
checkForUI

A helper function for exports.getFile. Looks for html files in the ui directory.

Returns: An error if a problem occurs, true if html files are found, or false if none are found.
*/

async function checkForUI() {

  const promise = new Promise(function(resolve, reject) {

    fs.readdir("./ui", function(error, files) {

      if (error) {
        reject(error);
      } else {
        if (files.length == 1) {
          if (files[0] == ".gitignore") {
            reject(".gitignore")
          }
        }
        //If anything at all is found other than a gitignore, we consider it to be a sign that a UI exists.
        resolve(true)
      }

    })

  });

  const result = await promise;
  return result;

}

/*
getFile

path: string, the requested file's location

headers: object, the request headers from the server call

routerCallback: function, a callback
  parameter 1: boolean, whether the file could be read
  parameter 2: object
    type: string, an indication of content-type
    data: string, the file contents

*/
exports.getFile = async function (request, path, headers, streamResponse, routerCallback) {
//VERY IMPORTANT: Relative paths for fs always reference process.cwd(), which is not necessarily the location of the file that required fs.

let uiPresent;

try {
  uiPresent = await checkForUI();
} catch (error) {
  uiPresent = false;
}
  

  //Strip out any URL parameters, or fileController will look for a filename that literally matches.
  path = path.split("?")[0];

  if (path === "/") {
    path = "/" + environmentVariables.defaultFile;
  }

  //This section builds API-style paths to try if an extension is unknown or missing.
  var extension = path.substring(path.lastIndexOf(".")+1);
  var knownType = false;

  u.forIn(environmentVariables.mimeTypes, function(index, mimeType) {

    if (mimeType[0].search(extension) >= 0) {
      knownType = mimeType[1];
    }

  });

  if (!knownType) {
    path = path + ".html";
  }

  //Determine the requested subdomain, and serve the UI for that subdomain, if the feature is turned on.
  var subdomain = null;

  try {
    if (environmentVariables.handlesSubdomains) {
      subdomain = "/" + headers.host.split(".")[0];
    } else {
      subdomain = "";
    }
  } catch (error) {
    subdomain = "";
  }

  //Make things work for people working/ developing on localhost, or with another host that doesn't have a TLD (TLDs are .com, .org, .dev, etc).
  //The pattern should match anything without a ., then a literal colon, and then anything after the colon. Everything before the colon should be captured.
  var pattern = /([^\.]+):.+/g
  var match = pattern.exec(subdomain);

  if (match) {
    subdomain = match[1];
  }

  if (path.indexOf("/") < 0) {
    path = "/" + path;
  }

  var fullPath = "./ui" + subdomain + path;

  if (uiPresent !== true) {
    subdomain = "";
    fullPath = "./coreLogic/basicUI" + subdomain + path;
  }


  if (knownType && knownType.indexOf("base64") >= 0) {

    //The streaming transport was built with code found here: https://medium.com/@daspinola/video-stream-with-node-js-and-html5-320b3191a6b6

    fs.stat(fullPath, function(error, fileStats) {

      if (error) {
        routerCallback(false, fullPath, null);
      } else {

        var fileSize = fileStats.size;
        var range = headers.range;

        if (range) {

          var parts = range.replace(/bytes=/, "").split("-");
          var start = parseInt(parts[0], 10);

          var end = parseInt(parts[1], 10);

          if (!end) {
            end = fileSize - 1;
          }

          var chunkSize = (end-start)+1;
          var streamable = fs.createReadStream(fullPath, {start, end});
          range = start + "-" + end + "/" + fileSize;

          const head = {
            "Content-Range": "bytes " + range,
            "Accept-Ranges": "bytes",
            "Content-Length": chunkSize,
            "Content-Type": knownType,
          }

          if (streamResponse) {

            streamResponse.writeHead(206, head);
            streamable.pipe(streamResponse);
            let entry = new LogEntry();
            entry.createFromResponse(streamResponse);

          } else {
            routerCallback(false, fullPath, null);
          }

        } else {

          const head = {
            "Content-Length": fileSize,
            "Content-Type": knownType,
          }

          if (streamResponse) {

            streamResponse.writeHead(200, head);
            fs.createReadStream(fullPath).pipe(streamResponse);
            let entry = new LogEntry();
            entry.createFromResponse(streamResponse);

          } else {
            routerCallback(false, fullPath, null);
          }

        }

      }

    });

  } else {

    fs.readFile(fullPath, function(error, data) {

      if (error) {

        //If the file couldn't be read, first try a managed file.
        var tryManagedFile = async function() {

          var managedFile = new ManagedUIFile();
          var fileName = path.substring(path.lastIndexOf("/")+1);

          try {
            managedFile = await managedFile.readFirstWithKeyValue({key: "fileName", value: fileName})

            if (!managedFile.data.id) {
              fullPath = `No managed UI file was found, and no file was found at ${fullPath}`;
              routerCallback(false, fullPath, null);
            } else if (managedFile.data.published !== true) {
              fullPath = `No file was found at ${fullPath}. A managed file was found in the core database, but it isn't published yet.`;
              routerCallback(false, fullPath, null);
            } else {
              handleFileContent(request, path, managedFile.data.content, headers, routerCallback);
            }
          } catch (error) {
            fullPath = `A database error occurred while looking up a managed UI file: ${error} Additionally, no file was found at ${fullPath}`;
            routerCallback(false, fullPath, null);
          }

        };

        tryManagedFile();

      } else {
        //If the file is found immediately, pass it to the handler.
        handleFileContent(request, path, data, headers, routerCallback);
      }

    });

  }

};
