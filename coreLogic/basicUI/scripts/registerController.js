var registerController = new zFrame("registerController");
registerController.useKeyFile = false;

//We might as well start using fetch, as it's more modern and behaves quite a bit like Zrequest anyway.
fetch(window.location.origin + "/api/loginModel/", {method: "GET", credentials: "include", cache: "reload"}).then(
	function(response) {
		if (response.ok) {
			response.json().then(
				function(json){
					if (json.message == "keyfile") {
						registerController.useKeyFile = true;
						registerController.PasswordGroup.hide();
						registerController.KeyfileGroup.show();
					}
				}
			)
		}
	}
);

registerController.validate = function() {

	var the = registerController;
	var allValid = true;

	if (!u.emailSeemsValid(the.Email.value)) {

		allValid = false;
		the.Email.addClass("invalid");

	} else {
		the.Email.removeClass("invalid");
	}

	if (the.useKeyFile == true) {

		if (!the.Keyfile.files[0]) {
			allValid = false;
			the.Keyfile.addClass("invalid");
		} else {
			the.Keyfile.removeClass("invalid");
		}

	} else {

		if (the.Password.value.length < 1) {
			allValid = false;
			the.Password.addClass("invalid");
		}
		else {
			the.Password.removeClass("invalid");
		}

		if (the.RepeatPassword.value !== the.Password.value) {

			allValid = false;
			the.RepeatPassword.addClass("invalid");
	
		} else {
			the.RepeatPassword.removeClass("invalid");
		}

	}
	
	if (allValid) {
		the.Submit.disabled = false;
	} else {
		the.Submit.disabled = true;
	}

};

registerController.Submit.onclick = function(event) {

	event.preventDefault();

	var payload = {};

	payload.email = registerController.Email.value;
	payload.password = registerController.Password.value;
	payload.repeatPassword = registerController.RepeatPassword.value;

	if (registerController.useKeyFile) {
		let reader = new FileReader();
		reader.readAsDataURL(registerController.Keyfile.files[0])
		reader.onload = function() {
			payload.keyfile = reader.result;
			submit(payload);
		}
	} else {
		submit(payload);
	}

	function submit(payload) {
		payload = JSON.stringify(payload);

		zRequest("POST", window.location.origin + "/api/users/", "application/json", [], payload, true, function(xhr) {
	
			var response = JSON.parse(xhr.response);
			alert(response.message);
	
		});
	}

};

registerController.attachUpdateEventsToInputs(registerController.validate);
