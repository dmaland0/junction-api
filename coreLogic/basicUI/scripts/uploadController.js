/*
global

*/

var uploadController = new zFrame("uploadController");

uploadController.File.onchange = function() {
	if (uploadController.File.files[0]) {
		uploadController.Submit.disabled = false;
	} else {
		uploadController.Submit.disabled = true;
	}
};

uploadController.Submit.onclick = function(event) {
	event.preventDefault();
	uploadController.Progress.innerHTML = "Encoding file...";

	var file = uploadController.File.files[0];
	var fileReader = new FileReader();
	var method = "POST";
	var url = "https://$host/api/upload/";
	url = url.replace("$host", window.location.host);
	fileReader.readAsDataURL(file);

	fileReader.onload = function() {

		//Remove the header from the file.
		var encodedFile = fileReader.result.substr(fileReader.result.indexOf("base64,") + 7);
		//Create a new payload object that can be stringified and submitted.
		var submitPayload = {};
		submitPayload.file = encodedFile;
		submitPayload.fileName = file.name;
		submitPayload = JSON.stringify(submitPayload);

		zRequest(method, url, "application/json", [{name: "upload-name", value: file.name}] , submitPayload, true, function(xhr) {
			var response = JSON.parse(xhr.response);
			alert(response.message);
		}, function(progress) {
			var percentage = Math.round(progress.loaded/progress.total * 100);
			uploadController.Progress.innerHTML = percentage + "%";
		});

	};

	fileReader.onerror = function() {
		//If there's a problem, take some notification action.
		alert("The specified file couldn't be read properly.");
	};

};
