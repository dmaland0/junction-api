//ZFrame is a Javascript experiment by Daniel Maland, dmaland0@gmail.com
//It's meant to do some of the nifty things that frameworks like Angular can do,
//but with more control and less overhead.

//If JSUtilities is missing, make it abundantly clear.
if (!window.JSUtilities) {
	var message = "zFramework requires JSUtilities to be loaded first. A version of JSUtilities (jsUtilities.js) should have been included with zFrameCore.js, if zFramework was cloned down from a repository. The JSUtilities GitLab page is here: https://gitlab.com/dmaland0/JSUtilities";
	console.log(message);

	var message = message.replace(/http.+/, '<a href="https://gitlab.com/dmaland0/JSUtilities" target="_blank">JSUtilities on GitLab</a>');
	var tag = '<h1 style="color:red;padding:5px;border:1px solid red;">$message</h1>';
	tag = tag.replace("$message", message);
	document.body.innerHTML = tag + document.body.innerHTML;
}

const u = JSUtilities;

//If this is false, console.log statements are bypassed.
var zFrameDebug = false;

/**
 * Used to instantiate a zFrame instance.
 * @param {String} name A string that will be searched for in HTML element IDs.
 * @example let inputForm = new zFrame('inputForm') 
 */
function zFrame(name) {

	this.name = name;

	if (!this.name) {
		this.name = "anonymous";
	}

	var body = document.getElementsByTagName('body')[0];
	var zFrameMatch = new RegExp(this.name + ".+");
	var thisZframe = this;

	this.stateObject = {};

	/**
	 * Update zFrame elements with the same name as stateObject entries using the stateObject values.
	 * HTML inputs have their values set. Other HTML tags have their innerHTML set.
	 * Null values are ignored.
	 */
	this.syncState = function () {
		var theZFrame = thisZframe;
		var stateObject = theZFrame.stateObject;

		for (elementName in stateObject) {

			if (stateObject.hasOwnProperty(elementName)) {
				if (stateObject[elementName] !== null) {
					let tagName = theZFrame[elementName].tagName
					if (tagName !== "INPUT" && tagName !== "TEXTAREA" && tagName !== "SELECT") {
						theZFrame[elementName].innerHTML = stateObject[elementName]
					} else {
						if (theZFrame[elementName].type == "checkbox") {
							if (stateObject[elementName]) {
								theZFrame[elementName].checked = true
							} else {
								theZFrame[elementName].checked = false
							}
						} else {
							theZFrame[elementName].value = stateObject[elementName]
						}						
					}
				}
			}
		}
	};

	/**
	 * Add a specified element and all children to this zFrame - only if their ID attributes contain a match for zFrame's name.
	 * @param {HTMLElement} element 
	 */
	this.addElementsToThisZFrame = function (element) {
		//Make sure this zFrame is available in child contexts.
		var theZFrame = thisZframe;

		if (element.id.match(zFrameMatch)) {
			var elementName = element.id.split(thisZframe.name)[1];

			if (elementName != "") {
				//Automatically attach showHide to everything, so it's as easy to use as calling instanceName.object.showHide().
				element.showHide = showHide;
				element.show = show;
				element.hide = hide;
				//As above, attach addClass and removeClass.
				element.addClass = addClass;
				element.removeClass = removeClass;

				thisZframe[elementName] = element;
				thisZframe.stateObject[elementName] = null;

				if (zFrameDebug) console.log(`Added ${element.id} to zFrame instance "${thisZframe.name}".`);
			}

		}

		if (element.children.length > 0) {

			u.forIn(element.children, function (index, child) {

				if (!u.valueIsIn(child, theZFrame).found) {
					//The recursion has a tendency to find elements more than once, so don't add an element that already exists.
					theZFrame.addElementsToThisZFrame(child);
				}

			});

		}

	};

	/**
	 * For each member that is an HTML input, attach the provided handlerFunction to BOTH the onchange and oninput events.
	 * @param {Function} handlerFunction 
	 */
	const attachUpdateEventsToInputs = function (handlerFunction) {

		u.forIn(this, function (index, entity) {

			if (entity.tagName) {

				if (entity.tagName == "INPUT" || entity.tagName == "SELECT" || entity.tagName == "TEXTAREA") {
					entity.onchange = handlerFunction;
					entity.oninput = handlerFunction;
					if (zFrameDebug) console.log(entity.id + " had onchange and oninput event handlers attached.");
				}

			}

		});

	};

	this.attachUpdateEventsToInputs = attachUpdateEventsToInputs;

	/**
	 * A function that's automatically attached to elements to make them easy to hide (or "undisplay," technically speaking.) The function toggles between a style.display of "" and "none."
	 */
	const showHide = function () {

		if (this.style.display != "none") {
			this.style.display = "none";
		} else {
			this.style.display = "";
		}

	};

	/**
	 * A function that's automatically attached to elements to make them easy to show or "display."
	 */
	const show = function() {
		this.style.display = "";
	}

	/**
	 * A function that's automatically attached to elements to make them easy to hide or "undisplay."
	 */
	const hide = function() {
		this.style.display = "none";
	}

	/**
	 * Shorthand function to add a class name to an HTML element.
	 * @param {String} className 
	 */
	const addClass = function (className) {

		if (this.classList && this.classList.add) {
			this.classList.add(className);
		}

	};

	/**
	 * Shorthand function to remove a class name from an HTML element.
	 * @param {String} className 
	 */
	const removeClass = function (className) {

		if (this.classList && this.classList.remove) {
			this.classList.remove(className);
		}

	};



	/*
	Example:
	
	var fruits = ["Apples", "Bananas", "Cherries"];
	controller.repeatTemplate(controller.ListItem, fruits, function(item, arrayIndex, fruit) {
		item.innerHTML = fruit;
	});
	*/

	/**
	* Repeat an element template within its parent, using members of an iterable (array, object, number) to create the new elements.
	* New elements get their IDs from the template's original ID, with the iterable index appended.
	*
	* The generationFunction is called for each iterable member, and is used for such things as updating dynamic values in the template.
	* GenerationFunction takes the newly created element, the iteration index, and the iteration value as parameters.
	* @param {HTMLElement} template 
	* @param {*} iterable 
	* @param {Function} generationFunction
	* @example var fruits = ["Apples", "Bananas", "Cherries"];
	* controller.repeatTemplate(controller.ListItem, fruits, function(newListItem, arrayIndex, fruit) {
	* newListItem.innerHTML = fruit; });
	*/
	this.repeatTemplate = function (template, iterable, generationFunction) {

		//Update an element ID, using recursion to find all children.
		//Also, append children with IDs to the parent as an object for easy access later, using the ID as the object index.
		//Example: child.id = "foo", rootElement.foo == child.
		function updateChildIDsAndAppend(rootElement, element, index) {

			if (element.id != "") {
				element.id = element.id + index;
			}

			u.forIn(element.children, function (childIndex, child) {

				if (child.id != "") {
					rootElement[child.id] = child;
				}

				updateChildIDsAndAppend(rootElement, child, index);

			});

		}

		var parent = template.parentElement;
		var children = [];

		//This is necessary because removing children causes iteration through parent.children to fail.
		//That is, parent.removeChild causes the child to disappear from the iterable object while iteration is still happening.
		u.forIn(parent.children, function (index, child) {
			children.push(child);
		});

		//First, autoflush any previously generated elements.
		u.forIn(children, function (index, child) {

			if (child != template && child.id.indexOf(template.id) >= 0) {
				parent.removeChild(child);
			}

		});

		//Build the new copies.
		u.forIn(iterable, function (index, value) {

			var newElement = template.cloneNode(true);

			updateChildIDsAndAppend(newElement, newElement, index);
			generationFunction(newElement, index, value);

			if (newElement.style) {
				//Fix the problem of new elements inheriting a display style of none from the template.
				newElement.style.display = null;
			}

			parent.appendChild(newElement);

		});

		//Hide the template.
		template.style.display = "none";
		if (zFrameDebug) console.log(template.id + " was used as a template for repeated elements, and automatically hidden.");

	};

	//Automatically try to populate this zFrame upon instantiation.
	this.addElementsToThisZFrame(body);

}

/**
 * A helper function to make XHR requests easier to perform.
 * @param {String} method The request verb, like "GET", "POST", "DELETE", etc.
 * @param {String} url The address to make the request to, like "https://developer.mozilla.org".
 * @param {String} contentType The mime type of the request, like "application/json".
 * @param {Array} otherHeaders otherHeaders: An array of header objects, like [	{name: "header", value: "stuff"}, {name: "anotherHeader", value: "moreStuff"} ]
 * @param {*} payload The request body, which will be sent as-is.
 * @param {Boolean} useCredentials Whether or not to send cookies, auth headers, etc. with cross-site requests.
 * @param {Function} callBack The function to call with the XHR request as a parameter. (Used to act on the response when the request is fulfilled or fails.)
 * @param {Function} progressCallback The function to call when the upload.onprogress event fires. (Useful for updating progress bars and such.)
 */
function zRequest(method, url, contentType, otherHeaders, payload, useCredentials, callBack, progressCallback) {

	if (!otherHeaders) {
		otherHeaders = [];
	}

	var xhr = new XMLHttpRequest();
	xhr.withCredentials = useCredentials;

	xhr.upload.onprogress = function (e) {
		if (progressCallback) {
			progressCallback(e);
		}
	};

	xhr.addEventListener("readystatechange", function () {
		if (this.readyState === 4) {
			callBack(xhr);
		}
	});

	xhr.open(method, url);

	if (method != 'GET' && method != 'DELETE') {
		xhr.setRequestHeader("Content-Type", contentType);
	}

	u.forIn(otherHeaders, function (index, header) {
		xhr.setRequestHeader(header.name, header.value);
	});

	xhr.send(payload);

}

/**
 * A zAlert is a global called with zAlert.show().
 */
var zAlert = document.createElement('div');

zAlert.initialized = false;
zAlert.defeatStyling = false;
zAlert.id = "zAlert";

/**
 * Sets up a zAlert.
 * @param {Boolean} defeatStyling Whether or not to allow the zAlert to override any applied styling.
 */
zAlert.initialize = function (defeatStyling) {

	if (zAlert.initialized) {
		console.log("ZAlert can only be initialized once.");
		return false;
	}

	zAlert.defeatStyling = defeatStyling;

	zAlert.heading = document.createElement('h1');

	if (!zAlert.defeatStyling) {

		zAlert.heading.style.color = 'black';
		zAlert.heading.style.textAlign = 'center';
		zAlert.heading.style.margin = '10px';

	}

	zAlert.appendChild(zAlert.heading);

	zAlert.icon = document.createElement('img');

	if (!zAlert.defeatStyling) {

		zAlert.icon.style.display = 'block';
		zAlert.icon.style.margin = '10px auto';

	}

	zAlert.appendChild(zAlert.icon);

	zAlert.body = document.createElement('p');

	if (!zAlert.defeatStyling) {

		zAlert.body.style.color = 'black';
		zAlert.body.style.margin = '10px 20px';

	}


	zAlert.appendChild(zAlert.body);

	zAlert.dismiss = document.createElement('div');
	zAlert.dismiss.innerHTML = "Dismiss";

	if (!zAlert.defeatStyling) {

		zAlert.dismiss.styles = {
			color: '#a52424',
			margin: '10px 10px 10px 10px',
			border: '1px solid #a52425',
			display: 'block',
			padding: '10px',
			borderRadius: '5px',
			position: 'absolute',
			bottom: '0px',
			right: '0px'
		};

		u.forIn(zAlert.dismiss.styles, function (key, value) {
			zAlert.dismiss.style[key] = value;
		});

	}

	zAlert.appendChild(zAlert.dismiss);

	zAlert.dismiss.onclick = function (event) {
		zAlert.hide();
	};

	zAlert.initialized = true;
	zAlert.hide();

};

/**
 * Apply necessary basic formatting to a zAlert.
 */
zAlert.format = function () {

	zAlert.style.position = 'fixed';
	zAlert.style.height = (window.innerHeight * 0.75) + "px";

	if (zAlert.defeatStyling) {
		return null;
	} else {

		zAlert.styles = {
			width: '75%',
			backgroundColor: 'white',
			boxShadow: '2px 2px 10px black',
			borderRadius: '5px',
			left: ((window.innerWidth - window.innerWidth * 0.75) / 2) + "px",
			transition: 'all 1s',
		};

		u.forIn(zAlert.styles, function (key, value) {
			zAlert.style[key] = value;
		});

	}

};

/**
 * An action to take when a zAlert is hidden.
 */
zAlert.onhide = function () {

};

/**
 * Hide a zAlert and execute zAlert.onhide.
 */
zAlert.hide = function () {

	if (!zAlert.initialized) {
		console.log("You must call zAlert.initialize() before calling zAlert.hide().");
		return false;
	}

	zAlert.style.top = (window.innerHeight * -1) + "px";
	zAlert.onhide();

};

/**
 * Display a zAlert.
 * @param {String} heading The text for the H1 tag.
 * @param {String} icon A path for the alert's img src attribute.
 * @param {String} body Text for the alert P tag.
 */
zAlert.show = function (heading, icon, body) {

	if (!zAlert.initialized) {
		console.log("You must call zAlert.initialize() before calling zAlert.show().");
		return false;
	}

	var alertWidth = zAlert.getBoundingClientRect().width;

	if (!zAlert.defeatStyling) {

		zAlert.heading.style.fontSize = Math.min(alertWidth * .1, 50) + "px";
		zAlert.body.style.fontSize = Math.min(alertWidth * .08, 25) + "px";
		zAlert.body.style.textAlign = "left";
		zAlert.icon.style.maxWidth = (alertWidth * .1) + "px";
		zAlert.dismiss.style.fontSize = zAlert.heading.style.fontSize;

	}

	zAlert.heading.innerHTML = heading;
	zAlert.body.innerHTML = body;
	zAlert.icon.src = icon;

	zAlert.style.width = '75%';
	zAlert.style.height = (window.innerHeight * 0.75) + "px";
	zAlert.style.left = ((window.innerWidth - window.innerWidth * 0.75) / 2) + "px";
	zAlert.style.top = "10px";

};

window.setTimeout(function () {
	document.body.appendChild(zAlert);
	zAlert.initialize();
	zAlert.format();
}, 250);
