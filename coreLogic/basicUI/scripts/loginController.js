var loginController = new zFrame("loginController");
loginController.useKeyFile = false;

//We might as well start using fetch, as it's more modern and behaves quite a bit like Zrequest anyway.
fetch(window.location.origin + "/api/loginModel/", {method: "GET", credentials: "include", cache: "reload"}).then(
	function(response) {
		if (response.ok) {
			response.json().then(
				function(json){
					if (json.message == "keyfile") {
						loginController.useKeyFile = true;
						loginController.PasswordGroup.hide();
						loginController.KeyfileGroup.show();
						loginController.PasswordResetLink.hide();
					}
				}
			)
		}
	}
);

loginController.validate = function() {

	var the = loginController;
	var allValid = true;

	if (!u.emailSeemsValid(the.Email.value)) {

		allValid = false;
		the.Email.addClass("invalid");

	} else {
		the.Email.removeClass("invalid");
	}

	if (the.useKeyFile) {
		if (!loginController.Keyfile.files[0]) {
			allValid = false;
			the.Keyfile.addClass("invalid");
		} else {
			the.Keyfile.removeClass("invalid");
		}

	} else {
		if (the.Password.value.length < 1) {

			allValid = false;
			the.Password.addClass("invalid");
	
		} else {
			the.Password.removeClass("invalid");
		}
	}	

	if (allValid) {
		the.Submit.disabled = false;
	} else {
		the.Submit.disabled = true;
	}

}

const loginSuccessEvent = new CustomEvent('loginSuccessEvent', {
	detail: null,
	bubbles: true,
	cancelable: true,
	composed: true
});

loginController.Submit.onclick = function(event) {

	event.preventDefault();

	var payload = {};

	payload.email = loginController.Email.value;
	payload.password = loginController.Password.value;
	
	if (loginController.useKeyFile) {
		let reader = new FileReader();
		reader.readAsDataURL(loginController.Keyfile.files[0])
		reader.onload = function() {
			payload.keyfile = reader.result;
			submit(payload);
		}
	} else {
		submit(payload);
	}

	function submit(input) {
		input = JSON.stringify(input);

		zRequest("PUT", window.location.origin + "/api/login/", "application/json", [], input, true, function(xhr) {
			var response = JSON.parse(xhr.response);
			alert(response.message);
	
			if (response.success) {
				document.dispatchEvent(loginSuccessEvent);
			}
	
		});
	}

};

loginController.attachUpdateEventsToInputs(loginController.validate);
