let socketCommunication = {};
var websocketActivityController = new zFrame("websocketActivity");
let counter = 1;

websocketActivityController.stateObject = {
    A: "...",
    B: "...",
    C: "...",
    General: "..."
}

/**
 * Send data to a socket connection.
 * @param {*} input 
 */
socketCommunication.sendData = function (input) {
    if (socketCommunication.open) {
        socketCommunication.socket.send(JSON.stringify(input));  
    }    
}

/**
 * Request a ping time in milliseconds, for servers supporting this request.
 * @param {*} pingTime 
 */
socketCommunication.setPing = function (pingTime) {
    socketCommunication.sendData({
        request: {
            action: "setPing",
            parameters: [pingTime]
        }
    });
}

/**
 * Initiate socket communication with the given ping time, assuming the server understands the request.
 * @param {*} host The URL of a Websocket server. Window.location.host by default if not specified.
 * @param {*} pingTime A cycle time defined in milliseconds.
 */
socketCommunication.openSocket = function (pingTime, host = window.location.host) {
    window.WebSocket = window.WebSocket || window.MozWebSocket;
    socketCommunication.socket = new WebSocket("wss://" + host);

    socketCommunication.socket.onopen = function(event) {
        console.log("A WebSocket connection was opened at " + new Date() + ".", event);
        websocketActivityController.stateObject.General = "A WebSocket connection was opened at " + new Date() + ".";
        websocketActivityController.syncState();
        socketCommunication.open = true;
        socketCommunication.setPing(pingTime);
    };

    /**
     * Handle errors.
     * @param {*} error 
     */
    socketCommunication.socket.onerror = function(error) {
        console.log("There was a fatal error with the Websocket connection at " + new Date() + ". An error event was passed to this event handler, but depending on the error severity it may not have any useful data in it.", error);
        websocketActivityController.stateObject.General = "There was a fatal error with the Websocket connection at " + new Date() + ".";
        websocketActivityController.syncState();
    };

    let backgroundA = 'blue';
    let backgroundB = 'unset';

    function backgroundSwitch(element, background) {
        websocketActivityController[element].style.backgroundColor = background;
    }

    /**
     * Actions to take when data is received.
     * @param {*} incoming 
     */
    socketCommunication.socket.onmessage = function(incoming) {
        try {
            data = JSON.parse(incoming.data);
            console.table(data);

            //Only do this if the user interface it was built for is in use.
            if (websocketActivityController.A) {
                switch (counter) {
                    case 1:
                        websocketActivityController.stateObject.A = data.message;
                        backgroundSwitch('A', backgroundA);
                        counter++;
                        websocketActivityController.syncState();
    
                        window.setTimeout(function() {
                            backgroundSwitch('A', backgroundB);
                        }, 500);
    
                        break;
                
                    case 2:
                        websocketActivityController.stateObject.B = data.message;
                        backgroundSwitch('B', backgroundA);
                        counter++;
                        websocketActivityController.syncState();
                        
                        window.setTimeout(function() {
                            backgroundSwitch('B', backgroundB);
                        }, 500);
    
                        break;
    
                    case 3:
                        websocketActivityController.stateObject.C = data.message;
                        backgroundSwitch('C', backgroundA);
                        counter++;
                        websocketActivityController.syncState();
    
                        window.setTimeout(function() {
                            backgroundSwitch('C', backgroundB);
                        }, 500);
                        
                        break;
    
                    default:
                        websocketActivityController.stateObject.A = websocketActivityController.stateObject.B;
                        websocketActivityController.stateObject.B = websocketActivityController.stateObject.C;
                        websocketActivityController.stateObject.C = data.message;
                        backgroundSwitch('C', backgroundA);
                        counter = 0;
                        websocketActivityController.syncState();
                        
                        window.setTimeout(function() {
                            backgroundSwitch('C', backgroundB);
                        }, 500);
    
                        break;
                }
            }
            
        } catch (error) {
            console.log(error);
        }
    };

    socketCommunication.socket.onclose = function(event) {
        console.log("The Websocket connection was closed at " + new Date() + " . The event may or may not have useful information regarding the closure: ", event);
        websocketActivityController.stateObject.General = "The Websocket connection was closed at " + new Date() + " ."
        websocketActivityController.syncState();
    }
}

document.addEventListener('loginSuccessEvent', function(event) {
    socketCommunication.openSocket(10000);
});
