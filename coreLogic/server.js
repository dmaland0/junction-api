/****************************************
*****************************************
Dependencies*/

//Builtin And Third-Party Modules
const http = require("http");
const https = require("https");
const fs = require("fs");
const u = require("./utilities/jsUtilities").JSUtilities;
const cluster = require("cluster");
const os = require("os");
const crypto = require("crypto");
//Custom Modules
const LogEntry = require("./services/logger").LogEntry;
const router = require("./router");
const { MemoryBaseConnector } = require("./services/memoryBaseConnector");
const environmentVariables = require("../environmentVariables").environmentVariables;
const connections = require('./services/connections').connections;
const SocketResponseObject = require('./services/SocketResponseObject');
const UsersController = require('./controllers/usersController');
const websocketActions = require('./websocketActions');

/****************************************
*****************************************
Server Startup Logic*/

//Make sure a login model has been chosen.

let currentLoginModel = null

for (const model of Object.keys(environmentVariables.loginModel)) {
  if (environmentVariables.loginModel[model] === true) {
    currentLoginModel = model; 
  }
}

if (!currentLoginModel) {
  throw new Error(`No login model has been set to true in environmentVariables. You must specify a login model to launch the server.`);
} else {
  console.log(`\n\nThe current login model is ${currentLoginModel}.\n\n`)
}

//Create the processing cluster.

if (cluster.isMaster) {

  console.log(`Junction-API master process ${process.pid} started running at ${new Date()}\nWorking directory: ${process.cwd()}\n
  The configured port for insecure requests is: ${environmentVariables.insecureRequestPort}\n
  The configured port for secure requests is: ${environmentVariables.serverPort}\n\n`);

  let memoryBaseInUse = false;

  let connectionKeys = Object.keys(connections);

  connectionKeys.map((connection) => {
    let theConnection = connections[connection];
    if(theConnection instanceof MemoryBaseConnector) {
      memoryBaseInUse = true;
    }
  })

  if (environmentVariables.allowMultiprocessing) {

    if (!memoryBaseInUse) {
      for (let i = 0; i < os.cpus().length; i++) {
        var worker = cluster.fork();
        console.log("\nWorker process " + worker.process.pid + " forked.");
      }
    } else {
      console.warn("MemoryBase is in use for one or more database connections. MemoryBase does not work safely with multiprocessing. The multiprocessing cluster setup has been bypassed.")
    }
    
  }

  cluster.on("exit", (worker, code, signal) => {
    console.log("\nWorker process " + worker.process.pid + " died. Signal: " + signal);
  });

}

if ((cluster.isMaster && !environmentVariables.allowMultiprocessing) || (!cluster.isMaster && environmentVariables.allowMultiprocessing)) {

 /**
  * Crafts and sends an HTTP response to a client.
  * @param {Array} cookies An array of strings representing HTTP cookies.
  * @param {Integer} code An HTTP resposne code.
  * @param {String} contentType An HTTP content-type.
  * @param {*} body The HTTP response body, such as a text or binary file, or a JSON string.
  * @param {Object} response A Node HTTP response object.
  */
  var respond = function (cookies, code, contentType, body, response) {

    if(response.headersSent == true) {
      console.warn(`Respond in server.js was called, but headers were already sent. This may be happening because a controller is calling responseCallback 
      once, and then unexpectedly "falling through" to another call of responseCallback. (A common culprit is a responeCallback call below a try/ catch 
      block, where the catch triggers a response, but the function execution then continues.)`)
      return false;
    }

    /**
     * A wrapper to make log creation reusable.
     */
    function writeLog () {

      if (environmentVariables.keepLogs) {
        let entry = new LogEntry();
        entry.createFromResponse(response, body);
      }

    }

    //There's no reason to continue if no connection exists. Connections can fail to exist if a response is ended, and then
    //something attempts to use the same response again.
    if (response.connection) {

      response.setHeader("Cache-Control", "max-age=" + environmentVariables.cacheTimeInSeconds);

      if (cookies.length > 0) {
        response.setHeader("Set-Cookie", cookies);
      }

      if (!response.connection.parser) {

        response.connection.parser = {
          incoming: {
            headers: {
              origin: null
            }
          }
        };

      }

      //If the request is from an external origin, it will fail if we don't set certain headers. For security, we WANT the
      //request to fail if the external origin is not known to us in advance, so this block bypasses if the external origin
      //is not defined in environmentVariables.
      if (u.valueIsIn(response.connection.parser.incoming.headers.origin, environmentVariables.allowedExternalOrigins).found) {
        response.setHeader("Access-Control-Allow-Origin", response.connection.parser.incoming.headers.origin);
        response.setHeader("Access-Control-Allow-Credentials", "true");
        response.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
        response.setHeader("Access-Control-Allow-Headers", "Content-Type, Authorization");
      }

      response.writeHead(code, {"Content-Type": contentType});

      if (contentType.indexOf("base64") >= 0) {
        //Images (amongst other things) need to be encoded properly.
        response.end(body, "base64", writeLog);
      } else {
        response.end(body, null, writeLog);
      }

    } else {
      console.warn("Something tried to invoke a response without a connection to send the response to.\nThis isn't fatal, but your code may be doing something asynchronously that sends a response,\nand then continuing execution until something else tries to send one.");
    }

  };

  //Create a redirecting server for insecure requests.
  http.createServer((request, response) => {
      var host = request.headers.host;

      if (host) {
        host = host.split(":")[0];
      }

      if (environmentVariables.keepLogs) {
        let entry = new LogEntry();
        entry.createFromRequest(request);
      }

      respond([], 200, 'text/html', `Junction-API only serves meaningful responses over HTTPS (unless modified). Please try navigating to <a href="https://${host}:${environmentVariables.serverPort}">https://${host}:${environmentVariables.serverPort}</a>`, response)

  }).listen(environmentVariables.insecureRequestPort);

  //Create the actual server.

  //Load the key and certificate files, plus the certificate chain.

  try {

    var ignoreChain = false;

    var httpsOptions = {
      key: fs.readFileSync(environmentVariables.httpsKey),
      cert: fs.readFileSync(environmentVariables.httpsCertificate),
      ca: null
    };

  } catch (error) {

    var httpsOptions = {
      key: fs.readFileSync('./coreLogic/httpsBuiltin/key.pem'),
      cert: fs.readFileSync('./coreLogic/httpsBuiltin/certificate.pem'),
      ca: null
    };

    ignoreChain = true;

    console.warn(`===WARNING===\nDue to ${error}, any key, certificate, and certificate chain files specified in environmentVariables were ignored.\nThe builtin key and certificate files are being used. This is fine for testing, but very unsafe for production.\n`);
  
  }

  if (!ignoreChain) {
    u.forIn(environmentVariables.httpsChain, function(index, file) {
      var content = fs.readFileSync(file, "utf8");
      httpsOptions.ca += content;
    });
  }

  let httpsServer = https.createServer(httpsOptions, function (request, response) {

    if (environmentVariables.keepLogs) {
      let entry = new LogEntry();
      entry.createFromRequest(request);
    }

    /**
     * An internal function for "packaging" request handling.
     */
    async function consumeRequest() {
      //You have to set the encoding, or you get buffers from requests instead of something readable.
      request.setEncoding("utf-8");
      var allowedVerbs = ["GET","POST","PUT","DELETE", "OPTIONS"];

      if (request.method === "OPTIONS") {
        respond({cookies: [], code: 200, contentType: "text/plain", body: "OPTIONS request received.", response});
        return true;
      }

      if (u.valueIsIn(request.method, allowedVerbs).found) {
        //If the request used an allowed verb, continue.

        //Reading POST and PUT payloads requires catching data events and building a data reference to use later.
        var payload = "";

        request.on("data", function(chunk) {
          payload += chunk;
        });

        //When the request ends, try to parse the input and then send relevant data to the routing module.
        request.on("end", async function() {

          var failure = false;

          if (request.method === "POST" || request.method === "PUT") {

            try {
              payload = JSON.parse(payload);
            } catch (exception) {
              failure = true;
              var message = "A POST or PUT verb was used, but the supplied data couldn't be parsed as JSON:\n" + exception;
              respond([], 400, "text/plain", message, response);
            }

          }

          if (!failure) {

            //If everything's okay, call the router, and respond when the action is resolved.

            //VERY IMPORTANT: The callback and subsequent call to respond() are bypassed when a file is streamed directly to
            //the response object by fileController.js.
            router.route(request, request.headers, request.url, request.method, payload, response, (cookies, code, type, body) => {
              respond(cookies, code, type, body, response);
            });
            
          }

      });

      } else {
        //If the request did not use an allowed verb, fail immediately.
        var message = "You used the HTTP verb: " + request.method + ", which is not supported by this server.";
        respond([], 400, "text/plain", message, response);
      }

    }

    consumeRequest();

  }).listen(environmentVariables.serverPort);

  //Websockets implementation based on code from Tabjy
  //https://gist.github.com/tabjy/5b7d2c549cb29d9ac07a28aad1c9bfa4

  httpsServer.on('upgrade', function(request, socket) {
    let headers = request.headers;
    let pingTime = 10000;
    let pingInterval = null;

    if (headers.upgrade !== "websocket") {
      socket.end()
      socket.destroy()
      return;
    }

    const clientKey = headers["sec-websocket-key"];
    //Important: The UUID below is not random, nor can the hash type or digest method be changed. The WebSockets protocol is very strict in how to
    //generate a Sec-WebSocket-Accept header.
    const responseKey = crypto.createHash('sha1').update(clientKey + "258EAFA5-E914-47DA-95CA-C5AB0DC85B11", "binary").digest("base64");

    const responseHeaders = [
      'HTTP/1.1 101 Web Socket Protocol Handshake',
      'Upgrade: WebSocket',
      'Connection: Upgrade',
      `Sec-WebSocket-Accept: ${responseKey}`
    ];

    socket.write(responseHeaders.join("\r\n") + "\r\n\r\n");

    let userAuthenticated = false;
    let callObject = {};

    if (!headers.cookie) {
      headers.cookie = "";
    }

    let splitCookies = headers.cookie.split("; ");

    if (environmentVariables.allowAnonymousWebsocketConnections) {
      userAuthenticated = true;
      websocketContinue();
    } else {

      callObject = {
        request: request,
        parameters: null,
        payload: null,
        cookies: splitCookies,
        requestHeaders: headers
      };

      UsersController.authenticateWithToken(callObject, function(success, code, cookies, message, data) {
        if (!success) {
          userAuthenticated = false;
          websocketContinue();
        } else {
          userAuthenticated = true;
          socket.user = data;
          websocketContinue();
        }
      });
    }

    function websocketContinue() {
      if (userAuthenticated) {

        let responseObject = new SocketResponseObject.instance({
          success: true,
          code: 200,
          message: `WebSocket connection initiated. You can set a ping time by sending a message with an object similar to the following, where the number is changed to reflect the number of milliseconds between each ping: { request: { action: "setPing", parameters: [30000] } }`,
          data: null
        });

        socket.write(constructReply(responseObject));
        let entry = new LogEntry();
        entry.create(`Websocket ${JSON.stringify(socket.address())} was connected.`);

      } else {
        let responseObject = new SocketResponseObject.instance({
          success: false,
          code: 401,
          message: `WebSocket server contacted. The request succeeded, but this server does not allow unauthenticated users to use Websockets. You should try logging in normally first. The socket connection will be terminated after this message is sent.`,
          data: null
        });

        socket.write(constructReply(responseObject), function(error) {
          socket.end();
          socket.destroy();
        });

        let entry = new LogEntry();
        entry.create(`Websocket ${JSON.stringify(socket.address())} was connected, but this server automatically terminates Websocket connections from unauthenticated users. This can be changed via environmentVariables.`);

      }
    }

    /**
     * Mostly the original parser from Tabjy, with very minor tweaks to require masking.
     * @param {*} buffer 
     * @returns A string.
     * @throws Errors related to masking or large payloads.
     */
    function parseMessage (buffer) {
      const firstByte = buffer.readUInt8(0)
      // const isFinalFrame = Boolean((firstByte >>> 7) & 0x1)
      // const [reserved1, reserved2, reserved3] = [
      //   Boolean((firstByte >>> 6) & 0x1),
      //   Boolean((firstByte >>> 5) & 0x1),
      //   Boolean((firstByte >>> 4) & 0x1)
      // ]
      const opCode = firstByte & 0xf
      // We can return null to signify that this is a connection termination frame
      if (opCode === 0x8) return null
      // We only care about text frames from this point onward
      if (opCode !== 0x1) return
      const secondByte = buffer.readUInt8(1)
      const isMasked = Boolean((secondByte >>> 7) & 0x1)
      // Keep track of our current position as we advance through the buffer
      let currentOffset = 2
      let payloadLength = secondByte & 0x7f
      if (payloadLength > 125) {
        if (payloadLength === 126) {
          payloadLength = buffer.readUInt16BE(currentOffset)
          currentOffset += 2
        } else {
          // 127
          // If this has a value, the frame size is ridiculously huge!
          // const leftPart = buffer.readUInt32BE(currentOffset)
          // const rightPart = buffer.readUInt32BE((currentOffset += 4))
          // Honestly, if the frame length requires 64 bits, you're probably doing it wrong.
          // In Node.js you'll require the BigInt type, or a special library to handle this.
          throw new Error('Large payloads not currently implemented')
        }
      }
  
      let maskingKey
      if (isMasked) {
        maskingKey = buffer.readUInt32BE(currentOffset)
        currentOffset += 4
      } else {
        throw new Error('Websocket masking is required on this server.')
      }
  
      // Allocate somewhere to store the final message data
      const data = Buffer.alloc(payloadLength)
      // Only unmask the data if the masking bit was set to 1
      if (isMasked) {
        // Loop through the source buffer one byte at a time, keeping track of which
        // byte in the masking key to use in the next XOR calculation
        for (let i = 0, j = 0; i < payloadLength; ++i, j = i % 4) {
          // Extract the correct byte mask from the masking key
          const shift = j === 3 ? 0 : (3 - j) << 3
          const mask = (shift === 0 ? maskingKey : maskingKey >>> shift) & 0xff
          // Read a byte from the source buffer
          const source = buffer.readUInt8(currentOffset++)
          // XOR the source byte and write the result to the data
          data.writeUInt8(mask ^ source, i)
        }
      }
  
      return data.toString('utf8')

    }

    /**
     * The original reply constructor from Tabjy.
     * @param {*} data 
     * @returns 
     */
    function constructReply (data) {
      // Convert the data to JSON and copy it into a buffer
      const json = JSON.stringify(data)
      const jsonByteLength = Buffer.byteLength(json)
      // Note: we're not supporting > 65535 byte payloads at this stage
      const lengthByteCount = jsonByteLength < 126 ? 0 : 2
      const payloadLength = lengthByteCount === 0 ? jsonByteLength : 126
      const buffer = Buffer.alloc(2 + lengthByteCount + jsonByteLength)
      // Write out the first byte, using opcode `1` to indicate that the message
      // payload contains text data
      buffer.writeUInt8(0b10000001, 0)
      buffer.writeUInt8(payloadLength, 1)
      // Write the length of the JSON payload to the second byte
      let payloadOffset = 2
      if (lengthByteCount > 0) {
        buffer.writeUInt16BE(jsonByteLength, 2)
        payloadOffset += lengthByteCount
      }
      // Write the JSON data to the data buffer
      buffer.write(json, payloadOffset)
      return buffer
    }

    /**
     * Sets up or reconfigures a ping from the server to the client to hold a connection open.
     */
    function ping () {
      if (pingInterval) {
        clearInterval(pingInterval);
      }
      pingInterval = setInterval(() => {
        if (socket.writable) {

          let responseObject = new SocketResponseObject.instance({
            success: true,
            code: 200,
            message: `Ping at server time ${new Date()}.`,
            data: null
          });

          socket.write(constructReply(responseObject));
        }
      }, pingTime);
    } 

    socket.on('data', (buffer => {

      let message;

      try {
        message = parseMessage(buffer);
        message = JSON.parse(message);
      } catch (error) {
        socket.write(constructReply({ message: error }));
        return;
      }

      if (message === null) {

        let entry = new LogEntry();
        entry.create(`Websocket ${JSON.stringify(socket.address())} received a message that was null when parsed. Terminating connection.`);

        let responseObject = new SocketResponseObject.instance({
          success: false,
          code: 400,
          message: `The server received a WebSocket communication where the message was null. This is interpreted as a disconnection request.`,
          data: null
        });

        socket.write(constructReply(responseObject), function(error) {
          socket.end();
          socket.destroy();
        });

      } else if (message.request) {

        let entry = new LogEntry();
        entry.create(`Websocket ${JSON.stringify(socket.address())} sent a message requesting action ${message.request.action}`);

        let responseObject = new SocketResponseObject.instance({
          success: false,
          code: 999,
          message: `Unset.`,
          data: null
        });
        
        websocketActions.execute(socket, message, responseObject, function(theSocket, theResponse) {
          if (theSocket.writable) {
            //Special case for pings.
            if (theResponse.data) {
              if (theResponse.data.pingTime) {
                pingTime = theResponse.data.pingTime;
                ping();
              }
            }
            theSocket.write(constructReply(theResponse));
            entry.create(`Websocket ${JSON.stringify(theSocket.address())} was sent a reply with message: ${theResponse.message}`);
          } else {
            entry.create(`Websocket ${JSON.stringify(theSocket.address())} was no longer writable when the response callback executed.`);
          }
        });

      } else {

        let responseObject = new SocketResponseObject.instance({
          success: false,
          code: 400,
          message: `Message.request cannot be empty.`,
          data: null
        });

        socket.write(constructReply(responseObject));
        let entry = new LogEntry();
        entry.create(`Websocket ${JSON.stringify(socket.address())} sent a message where message.request was empty.`);

      }

    }));

    socket.on('close', function() {
      let entry = new LogEntry();
      entry.create(`Websocket ${JSON.stringify(socket.address())} was closed.`);
      socket.end();
      socket.destroy();
    });

    socket.on('error', function(error) {
      let entry = new LogEntry();
      entry.create(`Websocket ${JSON.stringify(socket.address())} encountered an error: ${error}`);
      socket.end();
      socket.destroy();
      console.log(`Websocket ${JSON.stringify(socket.address())} encountered an error: ${error}`);
    });

  });

}
