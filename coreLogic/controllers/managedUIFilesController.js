/****************************************
*****************************************
Dependencies*/

//Builtin And Third-Party Modules
const u = require("../utilities/jsUtilities").JSUtilities;
//Custom Modules
const managedUIFile = require('../dataObjects/ManagedUIFile').ManagedUIFile

/****************************************
*****************************************
Module Logic*/

exports.getOne = async function(callObject, resultCallback) {

  var managedFile = new managedUIFile();

  try {
    managedFile = await managedFile.readFirstWithKeyValue({key: "fileName", value: callObject.parameters[0]});
  } catch (error) {
    resultCallback(false, 500, [], "There was an error while reading a managed UI file.", error);
    return
  }

  if (!managedFile.data.id) {
    resultCallback(false, 404, [], "No managed UI file was found with the name: " + callObject.parameters[0], null);
  } else {
    resultCallback(true, 200, [], "A managed UI file was found.", managedFile.data);
  }

};

exports.getOnePublished = async function(callObject, resultCallback) {

  var managedFile = new managedUIFile();

  try {
    managedFile = await managedFile.readFirstWithKeyValue({key: "fileName", value: callObject.parameters[0]});
  } catch (error) {
    resultCallback(false, 500, [], "There was an error while reading a managed UI file.", error);
    return
  }

  if (!managedFile.data.id || managedFile.data.published == false) {
    resultCallback(false, 404, [], "No publicly available managed UI file was found with the name: " + callObject.parameters[0], null);
  } else {
    resultCallback(true, 200, [], "A managed UI file was found.", managedFile.data);
  }

};

exports.list = async function(callObject, resultCallback) {
  var managedFiles = new managedUIFile()

  try {
    managedFiles = await managedFiles.readAll();
  } catch (error) {
    resultCallback(false, 500, [], "There was an error while reading managed UI files.", error);
    return
  }

  let list = [];

  managedFiles.map((file) => {
    try {
      list.push(file.data.fileName);
    } catch (error) {
      list = list;
    }
  });

  list.sort();

  resultCallback(true, 200, [], "A list of managed UI files was generated.", list);

};

exports.listPublished = async function(callObject, resultCallback) {
  var managedFiles = new managedUIFile()

  try {
    managedFiles = await managedFiles.readAllWithKeyValue({key: "published", value: true});
  } catch (error) {
    resultCallback(false, 500, [], "There was an error while reading managed UI files.", error);
    return
  }

  let list = [];

  managedFiles.map((file) => {
    try {
      list.push(file.data.fileName);
    } catch (error) {
      list = list;
    }
  });

  list.sort();

  resultCallback(true, 200, [], "A list of managed UI files was generated.", list);

};

exports.create = async function(callObject, resultCallback) {

  var file = new managedUIFile();
  file = await file.readFirstWithKeyValue({key: "fileName", value: callObject.payload.name});

  if (file.data.id) {
    resultCallback(false, 400, [], "Managed UI files must all have unique names. A record with the specified name already exists.", null);
    return
  } else {

    let fileName = callObject.payload.name;
    let content = callObject.payload.content;
    let published = false;

    try {
      await file.create({fileName, content, published});
    } catch (error) {
      resultCallback(false, 500, [], "There was an error while saving the managed UI file.", error);
    }
    
    resultCallback(true, 200, [], "A managed UI file was created.", null);

  }

};

exports.update = async function(callObject, resultCallback) {

  var file = new managedUIFile();

  try {
    file = await file.readFirstWithKeyValue({key: "fileName", value: callObject.parameters[0]});
  } catch (error) {
    resultCallback(false, 500, [], "An error occurred while looking for a managed UI file.", error);
    return false;
  }

  if (!file.data.id) {
    resultCallback(false, 404, [], "There is no managed UI file with that name.", null);
  } else {

    let content = callObject.payload.content;
    let published = Boolean(callObject.payload.published);

    try {
      await file.update({content, published});
    } catch (error) {
      resultCallback(false, 500, [], "An error occurred while updating a managed UI file.", error);
      return false;
    }

    resultCallback(true, 200, [], "A managed UI file was modified.", null);

  }

};

exports.delete = async function(callObject, resultCallback) {

  let file = new managedUIFile();

  try {
    file = await file.readFirstWithKeyValue({key: "fileName", value: callObject.parameters[0]});
  } catch (error) {
    resultCallback(false, 500, [], "An error occurred while reading a file.", error);
    return false;
  }
  
  if (!file.data.id) {
    resultCallback(false, 404, [], `There is no managed UI file with the name: ${callObject.parameters[0]}`, null);
  } else {

    try {
      await file.delete();
    } catch (error) {
      resultCallback(false, 500, [], "An error occurred while deleting a file.", error);
      return false;
    }

    resultCallback(true, 200, [], `A managed UI file, ${file.data.fileName}, was deleted.`, null);

  }

};
