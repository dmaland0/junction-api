/****************************************
*****************************************
Dependencies*/

//Builtin And Third-Party Modules
const u = require("../utilities/jsUtilities").JSUtilities;
const crypto = require("crypto");
//Custom Modules
const environmentVariables = require("../../environmentVariables").environmentVariables;
const User = require('../dataObjects/User').User;
const FailedLogin = require('../dataObjects/FailedLogin').FailedLogin;
const RegistrationKey = require('../dataObjects/RegistrationKey').RegistrationKey;
const DeviceKey = require('../dataObjects/DeviceKey').DeviceKey;
const PasswordResetKey = require('../dataObjects/PasswordResetKey').PasswordResetKey;
const mailer = require('../services/mailer');

/****************************************
*****************************************
Module Logic*/

/*
hashPassword

password: string, the actual password to be hashed

salt: string, a value to append to the password to make breaking the hashed value more difficult

Returns a hash digest.
*/
function hashPassword(password, salt) {

  const hash = crypto.createHash("sha512");
  hash.update(password + salt, "utf8");
  return hash.digest("hex");

}

/*
tokenExtractor

cookieArray: array of strings, the cookies from a request

Returns a bearer token, or null.
Bearer userToken=[value]; deviceKey=[value]

*/
function tokenExtractor (callObject) {

  let headers = callObject.requestHeaders
  let token = null

  try {

    if (headers.Authorization) {
      let header = headers.Authorization;
      rawTokens = headers.slice(header.lastIndexOf("Bearer "));
      rawUserToken = rawTokens.split(";")[0]
      token = rawUserToken.slice(rawUserToken.lastIndexOf("="))
    } else {
      callObject.cookies.map((cookie) => {
        if (cookie.indexOf("userToken") >= 0) {
          token = cookie.split("=")[1];
        }
      });
    }
  
    return token
    
  } catch (error) {
    return null
  }

}

/*
deviceKeyExtractor

cookieArray: array of strings, the cookies from a request

Returns a deviceKey, or null.

*/
function deviceKeyExtractor (callObject) {

  let headers = callObject.requestHeaders
  let deviceKey = null

  try {

    if (headers.Authorization) {
      let header = headers.Authorization;
      rawTokens = headers.slice(header.lastIndexOf("Bearer "));
      rawDeviceKey = rawTokens.split(";")[1];
      deviceKey = rawDeviceKey.slice(rawDeviceKey.lastIndexOf("="));
    } else {
      callObject.cookies.map((cookie) => {
        if (cookie.indexOf("deviceKey") >= 0) {
          deviceKey = cookie.split("=")[1];
        }
      });
    }
  
    return deviceKey;

  } catch (error) {
    return null;
  }

  

}

/*
sendDeviceKeyEmail

key: string, a device key.

callback: function, a callback
  parameter: boolean, whether the operation was successful
  parameter: string, a message regarding the operation

*/
function sendDeviceKeyEmail(key, request, user, emailCallback) {

  var pattern = /:.+/g;
  var host = request.headers.host.replace(pattern, "");
  var origin = request.headers.origin;

  //Some browsers don't set the origin header on POST requests, so we need a bit of a kludge.
  if (!origin) {
    origin = request.headers.referer;
  } else {
    origin = origin + "/";
  }

  var data = {
    from: "Email Notifications <api-noreply@" + host + ">",
    to: user.data.email,
    subject: "Device Key Authorization",
    text: "A login attempt was made on a device or browser that was unrecognized. A device key was created, which you can attempt to authorize at " + origin + "authorizeDevice?=" + key + " Do not authorize a device unless you yourself tried to login and were unable to complete the process. If this message is unexpected, someone has very likely discovered your password, and you should change or reset it immediately!"
  };

  mailer.sendMail(data, function(success, message) {

    if (!success) {
      emailCallback(false, message)
    } else {
      emailCallback(true, message);
    }

  });

}

/*
sendConfirmationEmail

key: string, the user's registration key.

callback: function, a callback
  parameter: boolean, whether the operation was successful
  parameter: string, a message regarding the operation

*/
async function sendConfirmationEmail(key, request, user) {

  var pattern = /:.+/g;
  var host = request.headers.host.replace(pattern, "");
  var origin = request.headers.origin;

  //Some browsers don't set the origin header on POST requests, so we need a bit of a kludge.
  if (!origin) {
    origin = request.headers.referer;
  } else {
    origin = origin + "/";
  }

  var data = {
    from: "Email Notifications <api-noreply@" + host + ">",
    to: user.data.email,
    subject: "Registration Confirmation",
    text: "Your user registration at " + host + " is almost complete. You can confirm your registration at " + origin + "confirmRegistration?=" + key + " If you did not intend to register on the system, simply delete this email - nobody can log in as you without completing this confirmation."
  };

  return new Promise((resolve, reject) => {
    mailer.sendMail(data, function(success, message) {

      if (success) {
        resolve(message)
      } else {
        reject(message);
      }
  
    });
  });  

}

/*
sendResetEmail

key: string, the user's password reset key.

callback: function, a callback
  parameter: boolean, whether the operation was successful
  parameter: string, a message regarding the operation

*/
function sendResetEmail(finalKey, request, email, emailCallback) {

  var pattern = /:.+/g;
  var host = request.headers.host.replace(pattern, "");

  var data = {
    from: "Email Notifications <api-noreply@" + host + ">",
    to: email,
    subject: "Password Reset",
    text: "A password reset was requested for your account at " + host + ". The reset key is: " + finalKey + "\n\nYour password can not be reset without this key. If you've remembered your password, simply log in normally and the key will be discarded."
  };

  mailer.sendMail(data, function(success, message) {

    if (!success) {
      emailCallback(false, message)
    } else {
      emailCallback(true, message);
    }

  });

}

/*
getMany

callObject {
  parameters: array of strings, the "?" variables in an API route
  payload: object, data submitted to the server
  cookies: array of strings, the cookies found in the request
  requestHeaders: object, the headers from the request
}

resultCallback: function, a callback
  parameter: boolean, whether the operation was successful
  parameter: integer, the relevant HTTP status code
  parameter: array of strings, the cookies to be set
  parameter: string, a message about the operation
  parameter: array, a collection of user objects

*/
exports.getMany = async function (callObject, resultCallback) {

  var startIndex = callObject.parameters[0];
  var users = new User();

  try {
    users = await users.readMany({startID: parseInt(startIndex), limit: 100});
  } catch (error) {
    resultCallback(false, 500, [], "An error occurred while finding users.", error);
    return;
  }

  let output = [];

  users.map(async (user) => {
    user = user.protectSecretFields();
    output.push(user.data);
  });

  if (users.length < 1) {
    resultCallback(false, 404, [], "No users found when utilizing starting ID " + startIndex, null);
  } else {
    resultCallback(true, 200, [], "Users were retrieved.", output);
  }

};

/*
getOne

callObject {
  parameters: array of strings, the "?" variables in an API route
  payload: object, data submitted to the server
  cookies: array of strings, the cookies found in the request
  requestHeaders: object, the headers from the request
}

resultCallback: function, a callback
  parameter: boolean, whether the operation was successful
  parameter: integer, the relevant HTTP status code
  parameter: array of strings, the cookies to be set
  parameter: string, a message about the operation
  parameter: object, a user object

*/
exports.getOne = async function(callObject, resultCallback) {

  var user = new User();

  try {
    user = await user.readOne({id: callObject.parameters[0]});
  } catch (error) {
    resultCallback(false, 500, [], `An error occurred while retrieving a user.`, error);
  }

  user = await user.protectSecretFields();

  if (!user.data.id) {
    resultCallback(false, 404, [], "No users found with ID " + callObject.parameters[0], null);
  } else {
    resultCallback(true, 200, [], "A user was retrieved.", user.data);
  }

};

/*
getByToken

callObject {
  parameters: array of strings, the "?" variables in an API route
  payload: object, data submitted to the server
  cookies: array of strings, the cookies found in the request
  request: object, the entire, raw request received by the server
  requestHeaders: object, the headers from the request
}

resultCallback: function, a callback
  parameter: boolean, whether the operation was successful
  parameter: integer, the relevant HTTP status code
  parameter: array of strings, the cookies to be set
  parameter: string, a message about the operation
  parameter: object, a user object

*/
exports.getSelf = async function(callObject, resultCallback) {

  var token = tokenExtractor(callObject);
  var user = new User();

  try {
    user = await user.readFirstWithKeyValue({key: "accessToken", value:token});
  } catch (error) {
    resultCallback(false, 500, [], "There was an error while looking up users.", error);
    return
  }

  if (!user.data.id) {
    resultCallback(false, 404, [], "The supplied token couldn't be matched with an available user.", null);
  } else {

    user = await user.protectSecretFields();
    resultCallback(true, 200, [], "Retrieved the currently acting user.", user.data);

  }

};

exports.readWithQuery = async function(callObject, resultCallback) {
  let users = new User();
  let output = [];

  try {
    users = await users.readWithQuery({query: callObject.payload.query});

    users.map(async (user) => {
      //The below isn't necessary in this application, with superuser
      //privileges being required to run a flexible query, and
      //the use case for this query being limited in any case.
      //If the scope changes, this should be revisited, though.
      //user = user.protectSecretFields();
      output.push(user.data);
    });

  } catch (error) {
    resultCallback(false, 500, [], `An error occurred while retrieving users based on a query.`, error);
    return false;
  }

  if (output.length === 0) {
    resultCallback(false, 404, [], `No users found based on the query.`, null);
  } else {
    resultCallback(true, 200, [], `Users were retrieved based on a query.`, output);
  }

}

exports.getByEmail = async function(callObject, resultCallback) {

  var user = new User();

  try {
    user = await user.readFirstWithKeyValue({key: "email", value:callObject.parameters[0]});
  } catch (error) {
    resultCallback(false, 500, [], `There was an error while retrieving a user.`, error);
  }

  user = await user.protectSecretFields();

  if (!user.data.id) {
    resultCallback(false, 404, [], "No users found with email " + callObject.parameters[0], null);
  } else {
    resultCallback(true, 200, [], "A user was retrieved.", user.data);
  }

};

exports.create = async function(callObject, resultCallback) {

  let bypass = false;
  let tokenValue = tokenExtractor(callObject);
  let actingUser = new User();
  actingUser = await actingUser.readFirstWithKeyValue({key: "accessToken", value: tokenValue});

  if (actingUser.data.id) {
    if (actingUser.data.isSuperuser) {
      bypass = true;
    }
  }

  if (environmentVariables.registrationDisabled) {
    if (!bypass) {
      resultCallback(false, 403, [], "This system is currently disallowing new user registrations, unless a superuser handles the registration directly.", null);
      return false;
    }
  }

  if (environmentVariables.loginModel.keyfile == true) {
    if (!callObject.payload.keyfile) {
      resultCallback(false, 400, [], "The server expected a keyfile, but there doesn't seem to be a keyfile in the payload.", null);
      return
    } else {
      //Just copy the keyfile into the password - requires no changes below.
      callObject.payload.password = callObject.payload.keyfile;
      callObject.payload.repeatPassword = callObject.payload.keyfile;
    }
  }

  var user = new User();
  let data = user.data;
  var email = callObject.payload.email;

  if (!u.emailSeemsValid(email)) {
    resultCallback(false, 400, [], "The specified email doesn't seem to be a valid address.", null);
  } else {
    let existingUser;
    try {
      existingUser = await user.readFirstWithKeyValue({ key: "email", value: email });
    } catch (error) {
      existingUser = {data:{}};
    }

    if (existingUser.data.id) {
      resultCallback(false, 400, [], "The specified email is already in use.", null);
    } else {
      if (callObject.payload.password !== callObject.payload.repeatPassword) {
        resultCallback(false, 400, [], "The two submitted passwords don't match.", null);
      } else {

        var lastUser = new User();

        try {
          lastUser = await lastUser.readLast();
        } catch (error) {
          lastUser = {data: {}};
        }

        //The first user to register is given superuser privileges.
        let isSuperuser = null;

        if (!lastUser.data.id) {
          isSuperuser = true;
        } else {
          isSuperuser = false;
        }

        let salt = crypto.randomBytes(128).toString("hex");
        let hashedPassword = hashPassword(callObject.payload.password, salt);
        let mustChangePassword = false;
        let createdAt = new Date();

        try {
          data.isSuperuser = isSuperuser;
          data.salt = salt;
          data.hashedPassword = hashedPassword;
          data.mustChangePassword = mustChangePassword;
          data.createdAt = createdAt;
          data.email = email;
          
          await user.create(data);
          user = await user.readFirstWithKeyValue({ key: "email", value: email });
        } catch (error) {
          resultCallback(false, 500, [], "The new user couldn't be saved and retrieved properly.", error);
        }

        if (environmentVariables.loginModel.keyfile == false) {

          //Create a key for registration confirmation.
          let key = crypto.randomBytes(128).toString("hex");
          var modifier = crypto.randomBytes(64).toString("hex");
          var finalKey = hashPassword(key, modifier);
          var registrationKey = new RegistrationKey();
          key = finalKey;
          let userID = user.data.id;

          try {
            save = await registrationKey.create({key, userID});
          } catch (error) {
            resultCallback(false, 500, [], "The registration couldn't be completed, because the Registration Key couldn't be saved.", error);
            try {
              await user.delete()
            } catch (error) {
              user = {}
            }
            return
          }

          //Create a device key.
          key = crypto.randomBytes(128).toString("hex");
          modifier = crypto.randomBytes(64).toString("hex");
          finalKey = hashPassword(key, modifier);
          var deviceKey = new DeviceKey();
          key = finalKey;
          userID = user.data.id;
          let authorized = true;
          let authorizationCode = "";

          try {
            await deviceKey.create({key, userID, authorized, authorizationCode});
          } catch (error) {
            resultCallback(false, 500, [], "The registration couldn't be completed, because the Device Key couldn't be saved.", error);
            try {
              await user.delete()
            } catch (error) {
              user = {}
            }
            return
          }

          let emailResult = null;

          try {
            emailResult = await sendConfirmationEmail(registrationKey.data.key, callObject.request, user);
          } catch (error) {
            emailResult = error;
          }

          var deviceKeyCookie = "deviceKey=" + finalKey + "; Secure; " + "Max-Age=" + (3.154 * 100000000) + ";" + " Path=/";
          var infoObject = {createdUser: user.data.id, developerMessage: "The included key can be used in an Authorization header. The Authorization header should be of the form: Bearer userToken=[value]; deviceKey=[value]", deviceKey: deviceKey.data.key};
          resultCallback(true, 200, [deviceKeyCookie], `User ${user.data.id} was created successfully. ${emailResult}`, infoObject);          
        } else {
          resultCallback(true, 200, [], `User ${user.data.id} was created successfully.`, null);
        }

      }

    }

  }

};

exports.login = async function(callObject, resultCallback) {

  let email = callObject.payload.email;
  var user = new User();

  try {
    user = await user.readFirstWithKeyValue({ key: 'email', value: email });
  } catch (error) {
    user.data = {};
  }


  //We'll only save this if the login fails.

  let ip = callObject.request.connection.remoteAddress;
  let time = new Date();

  let failedLogin = new FailedLogin({ip, email, time});

  if (!user.data.id) {

    try {
      await failedLogin.create();
    } catch (error) {
      resultCallback(false, 500, [], "A database error occurred while saving a Failed Login.", error);
      return
    }

    resultCallback(false, 404, [], "The database looked for a user with the email " + email + ", but found nothing.", null);

  } else {

    let hashedLoginPassword = null;

    if (environmentVariables.loginModel.keyfile == true) {
      if (callObject.payload.keyfile) {
        hashedLoginPassword = hashPassword(callObject.payload.keyfile, user.data.salt);
      } else {
        resultCallback(false, 400, [], "The server expected a keyfile, but there doesn't seem to be a keyfile in the payload.", null);
        return
      }      
    } else {
      hashedLoginPassword = hashPassword(callObject.payload.password, user.data.salt);
    }

    if (user.data.hashedPassword !== hashedLoginPassword) {

      try {
        await failedLogin.create();
      } catch (error) {
        resultCallback(false, 500, [], "A database error occurred while saving a Failed Login.", error);
        return
      }

      resultCallback(false, 400, [], "The supplied secret (password or keyfile) doesn't seem to be correct.", null);

    } else {

      //If the password matches, set the access token.
      var laterMultiplier = 1000*60*60*24*365;

      var createUniqueToken = async function() {
        var random = crypto.randomBytes(128).toString("hex");
        var now = new Date();
        var token = hashPassword(random, now.getTime());

        //This will become the expiration date for the token.
        var later = new Date(now.getTime() + laterMultiplier);
        user.data.accessToken = token;
        user.data.tokenExpires = later;
        user.data.lastLoginIP = callObject.request.connection.remoteAddress;
        user.data.lastLoginTime = new Date()

        var collision = new User();

        try {
          collision = await collision.readFirstWithKeyValue({ key: 'accessToken', value: token });
        } catch (error) {
          collision.data = {};
        }

        //If we make a new token, and it happens to already exist, try again.
        if (collision.id) {
          createUniqueToken();
        }

      };

      createUniqueToken();

      try {
        await user.update();
      } catch (error) {
        resultCallback(false, 500, [], "A database error occurred while updating a User.", error);
        return
      }

        var token = user.data.accessToken;
        var tokenCookie = "userToken=" + user.data.accessToken + "; Secure; " + "Max-Age=" + laterMultiplier / 1000 + ";" + " Path=/";
        var allFailedLogins = new FailedLogin();

      try {
        allFailedLogins = await allFailedLogins.readAll();
      } catch (error) {
        allFailedLogins = [];
      }

      var deleteErrors = [];

      allFailedLogins.map(async (instance) => {
        if (instance.data.ip === callObject.request.connection.remoteAddress || instance.data.email === callObject.payload.email) {
          try {
            await instance.delete();
          } catch (error) {
            deleteErrors.push(error);
          }
        }
      });

      if (deleteErrors.length > 0) {
        resultCallback(false, 500, [], "A database error occurred while removing Failed Logins.", deleteErrors);
        return;

      } else {

        if (environmentVariables.loginModel.keyfile == false) {

          var allRegistrationKeys = new RegistrationKey();

          try {
            allRegistrationKeys = await allRegistrationKeys.readAll();
          } catch (error) {
            resultCallback(false, 500, [], "A database error occurred while reading Registration Keys.", error);
            return
          }

          var found = false;

          allRegistrationKeys.map(async (instance) => {
            if (instance.data.userID === user.data.id) {
              found = true;
            }
          });

          
          if (found) {
            resultCallback(false, 400, [], "Users with unused registration keys can't login normally. If you didn't receive a registration confirmation email, check your spam folder or ask an admin to search the server's email logs.", null);
          } else {

            var allResetKeys = new PasswordResetKey();

            try {
              allResetKeys = await allResetKeys.readAll();
            } catch (error) {
              allResetKeys = [];
            }

            deleteErrors = [];

            allResetKeys.map(async (instance) => {
              if (instance.data.userID === user.data.id) {
                try {
                  await instance.delete();
                } catch (error) {
                  deleteErrors.push(remove);
                }
              }
            });

            if (deleteErrors.length > 0) {
              resultCallback(false, 500, [], "A database error occurred while removing Password Reset Keys.", deleteErrors);
            } else {

              callObject.user = user;

              exports.authenticateDevice(callObject, function(success, code, cookies, message, data) {

                var infoObject = {developerMessage: "The included key can be used in an Authorization header. The Authorization header should be of the form: Bearer userToken=[value]; deviceKey=[value]", userToken: token};

                if (!success) {
                  resultCallback(success, code, cookies, message, data);
                } else {
                  resultCallback(true, 200, [tokenCookie], "User " + user.data.id + " was logged in.", infoObject);
                }

              });

            }

          }

        } else {
          //If using keyfiles, all of the above secondary authentication is unnecessary.
          resultCallback(true, 200, [tokenCookie], "User " + user.data.id + " was logged in.", null);
        }

      }

    }

  }

};

exports.authenticateDevice = async function(callObject, resultCallback) {

  if (environmentVariables.bypassDeviceAuthentication) {

    resultCallback(true, 200, [], "Device key authorization was bypassed.", null);

    console.log("**********\nWARNING: Device key authorization is bypassed. \n" +
    "This is not recommended, as anyone able to steal a user's login information \n" +
    "can log in without a second authentication factor.\n" +
    "A user claiming to be " + callObject.user.email + " logged in from " + callObject.request.connection.remoteAddress +
    "\nAt server time " + new Date() + "\n**********");

    return false;

  }

  var keyValue = deviceKeyExtractor(callObject);

  if (!keyValue) {
    keyValue = "";
  }

  var user = new User();

  try {
    await user.readFirstWithKeyValue({key: "email", value: callObject.user.data.email});
  } catch (error) {
    resultCallback(false, 500, [], "An error occurred while looking up a user.", error);
    return
  }

  if (!user.data.id) {
    resultCallback(false, 404, [], "No user found with the supplied email.", null);
  } else {

    var deviceKey = new DeviceKey();

    try {
      await deviceKey.readFirstWithKeyValue({key: "key", value: keyValue});
    } catch (error) {
      resultCallback(false, 500, [], "An error occurred while looking up a device key.", error);
      return
    }

    let finalAuthorizationCode = null;

    if (!deviceKey.data.id || !deviceKey.data.authorized || (deviceKey.data.userID !== user.data.id) ) {

      if (!deviceKey.data.id || (deviceKey.data.userID !== user.data.id) ) {

        //Create a device key for the client/ browser used during registration.
        var key = crypto.randomBytes(128).toString("hex");
        var modifier = crypto.randomBytes(64).toString("hex");
        var authorizationCode = crypto.randomBytes(64).toString("hex");
        var finalDeviceKey = hashPassword(key, modifier);
        finalAuthorizationCode = hashPassword(authorizationCode, modifier);

        deviceKey.data.key = finalDeviceKey;
        deviceKey.data.authorizationCode = finalAuthorizationCode;
        deviceKey.data.authorized = false;
        deviceKey.data.userID = user.data.id;

        try {
          await deviceKey.create();
        } catch (error) {
          resultCallback(false, 500, [], "An error occurred while creating a device key.", error);
          return
        }

      } else {
        finalAuthorizationCode = deviceKey.data.authorizationCode;
      }

      var infoObject = {developerMessage: "The included key can be used in an Authorization header. The Authorization header should be of the form: Bearer userToken=[value]; deviceKey=[value]", deviceKey: deviceKey.data.key};
      sendDeviceKeyEmail(finalAuthorizationCode, callObject.request, user, (success, message) => {
        var deviceKeyCookie = "deviceKey=" + deviceKey.data.key + "; Secure; " + "Max-Age=" + (3.154 * 100000000) + ";" + " Path=/";
        resultCallback(false, 403, [deviceKeyCookie], "Your login has been blocked due to an expired, incorrect, unauthorized, or non-existent deviceKey. (A previously recognized device or browser can become unrecognized if its cookies are cleared, or if a user other than you logs in using that device. Another user using your device to register can also cause this to happen.) A key has been generated for this device/ browser, and an attempt was made at sending an authorization email:" + message, infoObject);
      });

    } else {
      resultCallback(true, 200, [], "An authorized device key was matched to a user.", null);
    }

  }

};

/*
authenticateWithToken

callObject {
  parameters: array of strings, the "?" variables in an API route
  payload: object, data submitted to the server
  cookies: array of strings, the cookies found in the request
  requestHeaders: object, the headers from the request
}

resultCallback: function, a callback
  parameter: boolean, whether the operation was successful
  parameter: integer, the relevant HTTP status code
  parameter: array of strings, the cookies to be set
  parameter: string, a message about the operation
  parameter: object or null, a partial user object, if the authentication was successful, or nothing if not

*/
exports.authenticateWithToken = async function(callObject, resultCallback) {

  var tokenValue = tokenExtractor(callObject);

  if (!tokenValue) {
    resultCallback(false, 400, [], "No token was submitted with this request. Try logging in again.", null);
  } else {

    var user = new User();

    try {
      user = await user.readFirstWithKeyValue({key: "accessToken", value: tokenValue});
    } catch (error) {
      resultCallback(false, 500, [], "An error occurred while reading users.", error);
      return
    }
    

    if (!user.data.id) {
      resultCallback(false, 404, [], "The supplied token couldn't be matched with an available user.", null);
    } else {

      var now = new Date().getTime();
      var tokenExpires = new Date(user.data.tokenExpires).getTime();

      if (now > tokenExpires) {
        resultCallback(false, 403, [], "The supplied token appears to have expired.", null);
      } else {

          if (user.data.mustChangePassword == true) {
            resultCallback(false, 403, [], "User " + user.data.id + " can't use token authentication until they change their password or keyfile.", null);
          } else {
            callObject.request.headers["user-id"] = user.data.id;
            callObject.request.headers["is-superuser"] = user.data.isSuperuser;
            resultCallback(true, 200, [], "User " + user.data.id + " was authenticated via their token.", {user: user.data.id, isSuperuser: user.data.isSuperuser});
          }

      }

    }

  }

};

exports.update = async function (callObject, resultCallback) {

  var user = new User();
  try {
    user = await user.readOne({id: callObject.parameters[0]});
  } catch (error) {
    resultCallback(false, 500, [], "An error occurred while reading a user.", error);
    return false;
  }
  
  if (!user.data.id) {
    resultCallback(false, 404, [], "No user was found with ID " + callObject.parameters[0], null);
  } else {

    var input = callObject.payload;

    let collision = new User();

    try {
      collision = await collision.readFirstWithKeyValue({key: "email", value: input.email});
    } catch (error) {
      resultCallback(false, 500, [], "An error occurred while modifying a user.", error);
    }

    if (collision.data.id && collision.data.id != callObject.parameters[0]) {
      resultCallback(false, 400, [], "An error occurred while modifying a user.", `The email ${input.email} is already in use.`);
      return false;
    }

    user.data.email = input.email;
    user.data.isSuperuser = Boolean(input.isSuperuser); 
    user.data.mustChangePassword = Boolean(input.mustChangePassword);

    try {
      await user.update();
    } catch (error) {
      resultCallback(false, 500, [], "An error occurred while storing the updated user.", error);
      return false;
    }

    user = await user.protectSecretFields();
    resultCallback(true, 200, [], "A user was updated.", user.data);

  }

};

exports.delete = async function(callObject, resultCallback) {

  var user = new User();

  try {
    user = await user.readFirstWithKeyValue({key: "id", value: parseInt(callObject.parameters[0])});
  } catch (error) {
    resultCallback(false, 500, [], "An error occurred while looking up a user.", error);
    return false
  }

  if (!user.data.id) {
    resultCallback(false, 404, [], "The supplied ID couldn't be matched with an available user.", null);
  } else {

    try {
      await user.delete();
    } catch (error) {
      resultCallback(false, 500, [], "An error occurred while deleting a user.", error);
      return false
    }

    resultCallback(true, 200, [], "User " + user.data.id + " was deleted from this system.", null);

  }

};

exports.changePassword = async function(callObject, resultCallback) {

  if (callObject.payload.newPassword === callObject.payload.oldPassword) {
    resultCallback(false, 400, [], "The new and old passwords must not be the same.", null);
  } else {

    if (callObject.payload.newPassword !== callObject.payload.repeatNewPassword) {
      resultCallback(false, 400, [], "Both entries for a new password must match.", null);
    } else {

      var user = new User();

      try {
        user = await user.readFirstWithKeyValue({key: "email", value: callObject.payload.email});
      } catch (error) {
        resultCallback(false, 500, [], "There was an error while searching for a user.", error);
        return false;
      }

      if (!user.data.id) {
          resultCallback(false, 404, [], "No user found with the provided email.", null);
      } else {
        var hashedInput = hashPassword(callObject.payload.oldPassword, user.data.salt);

        if (hashedInput !== user.data.hashedPassword) {
          resultCallback(false, 404, [], "The input doesn't match the current password.", null);
        } else {

          var newHashedPassword = hashPassword(callObject.payload.newPassword, user.data.salt);
          let mustChangePassword = false;
          let hashedPassword = newHashedPassword;

          try {
            await user.update({mustChangePassword, hashedPassword});
          } catch (error) {
            resultCallback(false, 500, [], "There was an error while updating a user.", error);
            return false;
          }

          resultCallback(true, 200, [], "The user's password was updated.", null);

        }

      }

    }

  }

};

/*
changePasswordWithKey

callObject {
  parameters: array of strings, the "?" variables in an API route
  payload: object, data submitted to the server
  cookies: array of strings, the cookies found in the request
  requestHeaders: object, the headers from the request
}

resultCallback: function, a callback
  parameter: boolean, whether the operation was successful
  parameter: integer, the relevant HTTP status code
  parameter: array of strings, the cookies to be set
  parameter: string, a message about the operation
  parameter: null

*/
exports.changePasswordWithKey = async function(callObject, resultCallback) {

  var key = callObject.payload.key;

  if (callObject.payload.newPassword !== callObject.payload.repeatNewPassword) {
    resultCallback(false, 400, [], "The new password and repeated version must match.", null);
  } else {

    let resetKey = new PasswordResetKey();

    try {
      resetKey = await resetKey.readFirstWithKeyValue({key: "key", value: key});
    } catch (error) {
      resultCallback(false, 500, [], "An error occurred while looking up a reset key.", error);
      return false;
    }

    if (!resetKey.data.id) {
      resultCallback(false, 400, [], "The provided key doesn't match any stored reset keys.", null);
    } else {

      let user = new User();

      try {
        user = await user.readOne({id: resetKey.data.userID})
      } catch (error) {
        resultCallback(false, 500, [], "An error occurred while looking up a user.", error);
        return false;
      }

      if (!user.data.id) {
        resultCallback(false, 404, [], "The user associated with the provided reset key couldn't be retrieved.", null);
      } else {

        var newHashedPassword = hashPassword(callObject.payload.newPassword, user.data.salt);
        user.data.hashedPassword = newHashedPassword;

        try {
          await user.update();
        } catch (error) {
          resultCallback(false, 500, [], "There was an error while updating a user.", error);
          return false;
        }

        resultCallback(true, 200, [], "User " + user.id + " had their password changed using a password reset key.", null);

      }

    }

  }

};

exports.requireSuperuser = async function (callObject, resultCallback) {
  var user = new User();
  var foundToken = tokenExtractor(callObject);

  try {
    user = await user.readFirstWithKeyValue({key: "accessToken", value: foundToken});
  } catch (error) {
    resultCallback(false, 500, [], `An error occurred while finding a user: ${error}`, null)
    return
  }

  if (user.data && user.data.id) {
    if (user.data.isSuperuser == false) {
      resultCallback(false, 403, [], "User " + user.data.id + " does not have superuser privileges, which are required for this operation.", null);
    } else {
      resultCallback(true, 200, [], "User " + user.data.id + " has superuser privileges.", null);
    }
  } else {
    resultCallback(false, 404, [], "The user's identity couldn't be positively determined based on their login token.", null);
  }

};

exports.confirmRegistration = async function (callObject, resultCallback) {

  let key = callObject.payload.key;

//Prevents empty keys from matching null values in the database.
  if (!key) {
    key = "Not Supplied";
  }

  let registrationKey = new RegistrationKey();

  try {
    registrationKey = await registrationKey.readFirstWithKeyValue({key: "key", value: key});
  } catch (error) {
    registrationKey = {data:{}};
  }
  
    if (!registrationKey.data.id) {
      resultCallback(false, 404, [], "The registration key supplied couldn't be found in the database.", null);
    } else {
      let deletion;
      try {
        await registrationKey.delete();
        resultCallback(true, 200, [], "A registration key was matched. The affected user can now log in normally.", null);
      } catch (error) {
        resultCallback(false, 500, [], "A database error occurred while removing a registration key.", error);
      }

    }

  };

/*
authorizeDevice
*/
exports.authorizeDevice = async function(callObject, resultCallback) {

  var code = callObject.payload.code;

  //If we neglect to do this, some database systems will match an empty string with a deviceKey that has
  //a null value for the authorizationCode.

  if (!code) {
    code = "Not Provided";
  }

  var deviceKey = new DeviceKey();
  try {
    deviceKey = await deviceKey.readFirstWithKeyValue({key:"authorizationCode", value:code});
  } catch (error) {
    resultCallback(false, 500, [], "An error occurred while reading a device key.", error);
    return
  }

  if (!deviceKey.data.id) {
    resultCallback(false, 404, [], "The authorization code supplied didn't match a device authorization key in the database.", null);
  } else {

    try {
      await deviceKey.update({authorized: true});
    } catch (error) {
      resultCallback(false, 500, [], "An error occurred while authorizing a device key.", error);
      return
    }

    resultCallback(true, 200, [], "A device key was authorized. The user with the assigned key can now login using the device/ browser bearing the key. If the device or browser was previously authorized by another user, it is now NOT authorized for that user any longer.", null);

  }

};

exports.passwordResetSendEmail = async function(callObject, resultCallback) {

  var email = callObject.payload.email;

  var user = new User();
  
  try {
    user = await user.readFirstWithKeyValue({key: "email", value: email});
  } catch (error) {
    user = {data: {}};
  }
  
  if (!user.data.id) {
    resultCallback(false, 404, [], "There is no user " + email + " available on this server.", null);
  } else {

    var key = crypto.randomBytes(20).toString("hex");
    var passwordReset = new PasswordResetKey();
    passwordReset.data.userID = user.data.id;
    passwordReset.data.key = key;

    try {
      await passwordReset.create();
    } catch (error) {
      resultCallback(false, 500, [], "There was a database error while saving a password reset key: " + save, error);
      return
    }

    sendResetEmail(key, callObject.request, email, function(success, message) {

      if (!success) {
        resultCallback(false, 500, [], "A password reset key was generated, but the confirmation email doesn't appear to have been sent successfully: " + message, null);
      } else {
        resultCallback(true, 200, [], "A password reset key was generated for: " + email + ". " + message, null);
      }

    });

  }

};

exports.loginModel = async function(callObject, resultCallback) {
  let currentLoginModel = null

  for (const model of Object.keys(environmentVariables.loginModel)) {
    if (environmentVariables.loginModel[model] === true) {
      currentLoginModel = model; 
    }
  }

  if (!currentLoginModel) {
    resultCallback(false, 404, [], "No login model found - this shouldn't be possible with a running server, as the server process should fail to start - but somehow this message was triggered.", null);
  } else {
    resultCallback(true, 200, [], `${currentLoginModel}`, null);
  }

};

exports.changeKeyfile = async function(callObject, resultCallback) {

  if (environmentVariables.loginModel.keyfile != true) {
    resultCallback(false, 403, [], `This function can't be invoked on a server where the keyfile login model isn't configured as active.`, null)
    return;
  }

  let input = callObject.payload;
  let actingUserID = parseInt(callObject.request.headers["user-id"]);
  let actingUser = new User();

  try {
    let targetUser = new User();
    let newSecret = null;

    actingUser = await actingUser.readOne({id: actingUserID});

    if (actingUser.data.email != input.email) {
      if (!actingUser.data.isSuperuser) {
        resultCallback(false, 403, [], `Only superusers can modify the keyfile of another user.` , null);
        return;
      } else {
        targetUser = await targetUser.readFirstWithKeyValue({key: "email", value: input.email});

        if (!targetUser.data.id) {
          throw `No user with email ${input.email} found on this system.`
        }

        newSecret = hashPassword(input.keyfile, targetUser.data.salt);
        targetUser.data.hashedPassword = newSecret;
        await targetUser.update();
        resultCallback(true, 200, [], `${targetUser.data.email} now has an updated keyfile.`, null);
      }
    } else {
      targetUser = actingUser;
      newSecret = hashPassword(input.keyfile, targetUser.data.salt);
      targetUser.data.hashedPassword = newSecret;
      await targetUser.update();
      resultCallback(true, 200, [], `${targetUser.data.email} now has an updated keyfile.`, null);
    }

  } catch (error) {
    resultCallback(false, 500, [], `An error occurred while changing a keyfile: ${error}`, null);
    return;
  }

};
