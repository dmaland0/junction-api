/****************************************
*****************************************
Dependencies*/

//Builtin And Third-Party Modules
//Custom Modules

/****************************************
*****************************************
Module Logic*/

exports.setPing = function (socket, message, responseObject, callback) {
    if (message.request.parameters[0]) {

        let pingTime = message.request.parameters[0];

        if (pingTime >= 10000) {

          responseObject.success = true;
          responseObject.code = 200;
          responseObject.message = `Server ping interval for this socket should now be ${pingTime} milliseconds.`;
          responseObject.data = {
            pingTime: pingTime
          }

        } else {

            responseObject.success = true,
            responseObject.code = 200,
            responseObject.message = `Server ping interval for this socket should now be 10000 milliseconds. The minimum ping time is 10 seconds, so your request for ${pingTime} milliseconds was overridden.`,
            responseObject.data = {
                pingTime: 10000
            }

        }
    } else {

        responseObject.success = true,
        responseObject.code = 200,
        responseObject.message = `Server ping interval for this socket should now be 10000 milliseconds. The minimum ping time is 10 seconds, so your request for an unspecified number of milliseconds was overridden.`,
        responseObject.data = {
            pingTime: 10000
        }

    }

    callback(socket, responseObject);
};

