/****************************************
*****************************************
Dependencies*/

//Builtin And Third-Party Modules
const u = require("../utilities/jsUtilities").JSUtilities;
const MemoryBaseConnector = require("./memoryBaseConnector").MemoryBaseConnector;
const userConnections = require("../../databaseConnections").connections;
//Custom Modules
const environmentVariables = require('../../environmentVariables').environmentVariables;

/****************************************
*****************************************
Module Logic*/

exports.connections = {};

if (environmentVariables.useEmbeddedMemoryBase) {
  exports.connections.core = new MemoryBaseConnector({fileNameBase: "core", connectionName: "core", writeFrequency: 10000});
}

let userConnectionKeys = Object.keys(userConnections);

userConnectionKeys.map((userConnection) => {
  if (exports.connections[userConnection]) {
    throw `A connection with the name ${userConnection} already exists.`
  } else {
    exports.connections[userConnection] = userConnections[userConnection];
  }
});
