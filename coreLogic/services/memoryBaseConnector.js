/****************************************
*****************************************
Dependencies*/

//Builtin And Third-Party Modules
const u = require("../utilities/jsUtilities").JSUtilities;
//Custom Modules
const logic = require('../memoryBase/memoryBaseLogic').requests;

/****************************************
*****************************************
Module Logic*/

class MemoryBaseConnector {
    /**
     * Create a new MemoryBase connector.
     * @param {*} input
     * @param {Integer} input.writeFrequency The number of milliseconds between each on-disk backup of the database.
     * @param {String} input.fileNameBase Files related to the database will either have this name, or this name plus a unique id.
     */
    constructor(input = {}) {
        this.writeFrequency = 30000;
        this.fileNameBase = "memoryBase";
        this.connectionName = "memoryBase";

        u.forIn(input, (key, value) => {
            this[key] = value;
        });

        /**
         * Communicate with a MemoryBase server.
         * @param {*} input An object.
         * @param {String} input.data A stringified dataObject to use in the operation.
         * @param {String} input.request The action to perform on the server.
         */
        this.callMemoryBase = async function(input) {

            return new Promise(async (resolve, reject) => {

                if (!(input.data instanceof Object)) {
                    reject(`Bad invocation of callMemoryBase: Input.data must be an object.`);
                    return;
                }

                if (typeof(input.request) !== 'string') {
                    reject(`Bad invocation of callMemoryBase: Input.request must be of type string.`);
                    return;
                }

                try {
                    let result = await logic[input.request](input.data);

                    if (result.error) {
                        reject(result.error);
                        return;
                    } else {
                        resolve(result);
                        return;
                    }
                } catch (error) {
                    reject(error);
                    return;
                }

            });

        };

        setTimeout(async () => {
            try {
                let response = await this.callMemoryBase({request: "handshake", data: {handshake: "handshake"}});
                console.log(`MemoryBaseConnector "${this.fileNameBase}" initial logic handshake succeeded: ${response.message}`);
            } catch (error) {
                console.error(`MemoryBaseConnector initial logic handshake failed: ${error}`)
                return;
            }

            try {
                let initializationObject = {writeFrequency: this.writeFrequency, fileNameBase: this.fileNameBase, filePath:this.filePath, connectionName: this.connectionName};
                let response = await this.callMemoryBase({request: "initialize", data: initializationObject});
                console.log(response.message);
            } catch (error) {
                console.error(`MemoryBaseConnector initialize call to server failed: ${error}`);
                return;
            }
           
        }, 1000);  
        
    }

    async buildTable() {
        throw (`WARNING: MemoryBase does not support pre-defining tables. Data consistency is up to the API developer.`)
    }

    /**
     * Create a new record.
     * @param {*} dataObject A dataObject to use for the operation.
     * @param {String} dataObject.table The table name where the record should reside.
     * @param {String} dataObject.data The information to be used for constructing the record.
     * @returns The input dataObject.
     * @throws An error if the operation fails.
     */
    async create(dataObject) {
        try {
            let result = await this.callMemoryBase({data: dataObject, request: "create"});
            return result.data;
        } catch (error) {
            throw (`Create operation failed: ${error}`)
        }
    }

    /**
     * Find a record based on its unique identifier.
     * @param {*} dataObject A dataObject to use for the operation.
     * @param {*} dataObject.data.id The unique identifier to be used to find the record.
     * @param {String} dataObject.table The table to be searched.
     * @returns An single result, which may be empty.
     * @throws An error if the requested table doesn't exist.
     */
    async readOne(dataObject) {
        try {
            let result = await this.callMemoryBase({data: dataObject, request: "readOne"});
            return result.data;
        } catch (error) {
            throw (`ReadOne operation failed: ${error}`)
        }
    }

    /**
     * Find the last record in a table.
     * @param {*} dataObject A dataObject to use for the operation.
     * @param {*} dataObject.data.id The unique identifier to be used to find the record.
     * @param {String} dataObject.table The table to be searched.
     * @returns An single result, which may be empty.
     * @throws An error if the requested table doesn't exist.
     */
    async readLast(dataObject) {
        try {
            let result = await this.callMemoryBase({data: dataObject, request: "readLast"});
            return result.data;
        } catch (error) {
            throw (`ReadLast operation failed: ${error}`)
        }
    }

    /**
     * Read several records from a database table.
     * @param {*} dataObject A dataObject to use for the operation.
     * @param {Integer} dataObject.start The ID of the first record to be retrieved.
     * @param {String} dataObject.table The table to retrieve records from.
     * @param {Integer} dataObject.limit The maximum number of records to retrieve.
     * @returns An array of results, which may be empty.
     * @throws An error if the requested table doesn't exist.
     */
    async readMany(dataObject) {
        try {
            let result = await this.callMemoryBase({data: dataObject, request: "readMany"});
            return result.data;
        } catch (error) {
            throw (`ReadMany operation failed: ${error}`)
        }
    }

    /**
     * Find the first record where the specified field matches the specified value.
     * @param {*} dataObject A dataObject to use for the operation.
     * @param {*} dataObject.key The field name to use when matching criteria.
     * @param {*} dataObject.value The value that the field must hold to match successfully.
     * @param {String} dataObject.table The table to retrieve records from.
     * @returns An array of results, which may be empty.
     * @throws An error if the requested table doesn't exist.
     */
    async readFirstWithKeyValue(dataObject) {
        try {
            let result = await this.callMemoryBase({data: dataObject, request: "readFirstWithKeyValue"});
            return result.data;
        } catch (error) {
            throw (`ReadFirstWithKeyValue operation failed: ${error}`)
        }
    }

    /**
     * Find all records where the specified field matches the specified value.
     * @param {*} dataObject A dataObject to use for the operation.
     * @param {*} dataObject.key The field name to use when matching criteria.
     * @param {*} dataObject.value The value that the field must hold to match successfully.
     * @param {String} dataObject.table The table to retrieve records from.
     * @returns An array of results, which may be empty.
     * @throws An error if the requested table doesn't exist.
     */
    async readAllWithKeyValue(dataObject) {
        try {
            let result = await this.callMemoryBase({data: dataObject, request: "readAllWithKeyValue"});
            return result.data;
        } catch (error) {
            throw (`ReadAllWithKeyValue operation failed: ${error}`)
        }
    }

    /**
     * Find all records that satisfy a query.
     * @param {*} dataObject A dataObject to use for the operation.
     * @param {*} dataObject.query The query object for the database engine to interpret.
     * @param {String} dataObject.table The table to retrieve records from.
     * @returns An array of results, which may be empty.
     * @throws An error if the requested table doesn't exist, or the query can't be interpreted.
     */
    async readWithQuery(dataObject) {
        try {
            let result = await this.callMemoryBase({data: dataObject, request: "readWithQuery"});
            return result.data;
        } catch (error) {
            throw (`ReadWithQuery operation failed: ${error}`)
        }
    }

    /**
     * Retrieve all the records from a given table.
     * @param {*} dataObject A dataObject to use for the operation.
     * @param {String} dataObject.table The table to retrieve records from.
     * @returns An array of results, which may be empty.
     * @throws An error if the requested table doesn't exist.
     */
    async readAll(dataObject) {
        try {
            let result = await this.callMemoryBase({data: dataObject, request: "readAll"});
            return result.data;
        } catch (error) {
            throw (`ReadAll operation failed: ${error}`)
        }
    }

    /**
     * Get the most recent records from the database that correspond to the dataObject's table.
     * @param {*} dataObject A dataObject to use for the operation.
     * @param {*} dataObject.limit The maximum number of records to retrieve.
     * @returns An array of retrieved objects, sorted as the most recent record being first.
     * @throws An error if the database operation fails.
     */
    async readMostRecent(dataObject) {
        try {
            let result = await this.callMemoryBase({data: dataObject, request: "readMostRecent"});
            return result.data;
        } catch (error) {
            throw (`ReadMostRecent operation failed: ${error}`)
        }
    }

    /**
     * Retrieve every database entry encountered when searching all fields for a given value.
     * @param {*} dataObject A dataObject to use for the operation.
     * @param {*} dataObject.value The value that a field must hold to match successfully.
     * @param {String} dataObject.table The table to retrieve records from.
     * @returns An array of results, which may be empty.
     * @throws An error if the requested table doesn't exist.
     */
     async readAllWithAnyKeyHavingValue(dataObject) {
        try {
            let result = await this.callMemoryBase({data: dataObject, request: "readAllWithAnyKeyHavingValue"});
            return result.data;
        } catch (error) {
            throw (`readAllWithAnyKeyHavingValue operation failed: ${error}`)
        }
    }

    /**
     * Modify a record.
     * @param {*} dataObject A dataObject to use for the operation.
     * @param {String} dataObject.table The table where the record resides.
     * @param {*} dataObject.data.id The unique identifier of the record to be modified.
     * @returns The modified dataObject.
     * @throws An error if the requested table or record doesn't exist.
     */
    async update(dataObject) {
        try {
            let result = await this.callMemoryBase({data: dataObject, request: "update"});
            return result.data;
        } catch (error) {
            throw (`Update operation failed: ${error}`)
        }
    }

    /**
     * Remove a record.
     * @param {*} dataObject A dataObject to use for the operation.
     * @param {String} dataObject.table The table where the record resides.
     * @param {*} dataObject.data.id The unique identifier of the record to be modified.
     * @returns The dataObject input to the operation.
     * @throws An error if the requested table or record doesn't exist.
     */
    async delete(dataObject) {
        try {
            let result = await this.callMemoryBase({data: dataObject, request: "delete"});
            return dataObject;
        } catch (error) {
            throw (`Delete operation failed: ${error}`)
        }
    }

}

exports.MemoryBaseConnector = MemoryBaseConnector;
