/****************************************
*****************************************
Dependencies*/

//Builtin And Third-Party Modules
const u = require("../utilities/jsUtilities").JSUtilities;
const connections = require("./connections").connections
//Custom Modules

/****************************************
*****************************************
Module Logic*/

class DataObject {

    /**
     *
     * @param {*} input Parameters for setting up a DataObject.
     * @param {String} input.connectionName  A name that corresponds to a connection in the connections service.
     * @param {String} input.table The name of a table that this DataObject will use.
     * @param {String} input.messageIdentifier A human-readable name for the DataObject.
     * @param {Object} input.data Initialization values for the DataObjects data fields.
     * @param {Object} input.dataTypesByKey A mapping of keys from input.data to types, such as "number" or "string."
     * @param {Array} input.secretFields An array of field names that can be redacted for more secure output.
     */

    constructor(input = {}) {
        this.connectionName = null;
        this.table = null;
        this.messageIdentifier = "Data Objects";
        this.data = {};
        this.dataTypesByKey = {};
        this.secretFields = [];

        let keys = Object.keys(this);

        keys.map((key) => {
            this[key] = input[key];
        })

    }

    /**
     * Transform raw data into DataObjects.
     * @param {*} input
     * @param {Array} input.results An array of database records to convert.
     * @returns An array of DataObjects.
     */
    convertResultsToDataObjects(input) {
        let returnArray = [];
        let the = this;

        u.forIn(input.results, function(index, result) {
            let classObject = new DataObject({connectionName: the.connectionName, table: the.table, messageIdentifier: the.messageIdentifier, data: result, dataTypesByKey: the.dataTypesByKey, secretFields: the.secretFields});
            returnArray.push(classObject);
        });

        return returnArray;
    }

    /**
     * Create a table within the database engine for this DataObject.
     * @returns True if the operation was successful.
     * @throws An error if the operation failed.
     */
    async initialize() {
        try {
            await connections[this.connectionName].buildTable({dataObject: this});
            return true;
        } catch (error) {
            let newError = `Initialize encountered an error: ${error}`;
            throw newError;
        }
    }

    /**
     * Redact fields that shouldn't make it to an output.
     * @returns A redacted object, with most dataObject functionality removed.
     * @throws An error if the operation failed.
     */
    protectSecretFields() {
        let clone;

        try {
            clone = JSON.parse(JSON.stringify(this));

            if (clone.secretFields) {
                clone.secretFields.map((fieldName) => {
                    clone.data[fieldName] = "PROTECTED";
                });
            }
        } catch (error) {
            let newError = `An error occurred while protecting secret fields: ${error}`;
            throw newError;
        }
        
        return clone;
    }

    /**
     * Ensures that fields in the data object match the types mapped in dataTypesByKey.
     * @returns False if no type mismatches were encountered, or true if they were.
     */
    checkForTypeError() {
        let typeError = false;
        u.forIn(this.data, (key, value) => {
            let type = this.dataTypesByKey[key];
            if (typeof(value) !== type) {
                if (typeof(value) !== "object") {
                    typeError = `The value of ${key} is not of type ${type}.`;
                }
            }
        });
        return typeError;
    }

    /**
     * Melds an input object with the corresponding fields on a dataObject.
     * @param input An object with keys that should match the dataObject's data definition.
     * @returns null
     * @throws An error if the merge can't be completed.
     */
    merge(input) {
        try {
            if (input) {
                let fields = Object.keys(input);
                fields.map((field) => {
                    if (input[field] !== undefined) {
                        this.data[field] = input[field];
                    }
                });
            }
        } catch (error) {
            throw `${error}`;
        }
    }

    /**
     * Create a new database entry from this DataObject.
     * @param input An object which should have keys that match the dataObject's data definition.
     * @returns The created object.
     * @throws An error if type checking fails or the database fails to create a record.
     */
    async create(input) {

        try {
            this.merge(input);
        } catch (error) {
            throw `There was an error with the merge operation on create: ${error}`;
        }

        let typeError = this.checkForTypeError();

        //Bail out.
        if (typeError) {
            throw typeError;
        }

        try {
            await connections[this.connectionName].create(this);
            return this;
        } catch (error) {
            let newError = `Create encountered an error: ${error}`;
            throw newError;
        }

    }

    /**
     * Retrieve the database entry with the given ID.
     * @param {*} input
     * @param {*} input.id The unique identifier or key to use.
     * @returns The retrieved object
     * @throws An error if the database operation fails.
     */
    async readOne(input) {
        this.data.id = input.id;

        try {
            let data = await connections[this.connectionName].readOne(this);
            this.data = data;
            return this;
        } catch (error) {
            let newError = `ReadOne encountered an error: ${error}`;
            throw newError;
        }

    }

    /**
     * Retrieve the first database entry encountered when searching for a field/ attribute with the given value.
     * @param {*} input
     * @param {*} input.key The field name to use.
     * @param {*} input.value The value the field must contain.
     * @returns The retrieved object.
     * @throws An error if the database operation fails.
     */
    async readFirstWithKeyValue(input) {
        this.key = input.key;
        this.value = input.value;

        try {
            let data = await connections[this.connectionName].readFirstWithKeyValue(this);
            this.data = data;
            return this;
        } catch (error) {
            let newError = `ReadFirstWithKeyValue encountered an error: ${error}`;
            throw newError;
        }
    }

    /**
     * Retrieve every database entry encountered when searching for a field/ attribute with the given value.
     * @param {*} input
     * @param {*} input.key The field name to use.
     * @param {*} input.value The value the field must contain.
     * @returns The retrieved objects.
     * @throws An error if the database operation fails.
     */
    async readAllWithKeyValue(input) {
        this.key = input.key;
        this.value = input.value;

        try {
            let records = await connections[this.connectionName].readAllWithKeyValue(this);
            let objects = this.convertResultsToDataObjects({results: records});
            return objects;
        } catch (error) {
            let newError = `ReadAllWithKeyValue encountered an error: ${error}`;
            throw newError;
        }
    }

    /**
     * Retrieve all database objects satisfying a query.
     * @parm input 
     * @param {*} input.query A query description object.
     * @example {
	"query": {
		"filters": [
            {
			    "key": "isSuperuser",
				"operator": "==",
				"value": true
			}
        ],
		"sort": {
			"key": "email",
            "direction": "descending"
		},
        "fieldsToInclude": [
            "id",
            "email"
        ],
		"slice": {
			"start": 0,
			"end": 100
		}
	}
} 
     * @returns The retrieved objects.
     * @throws An error if the database operation fails.
     */
    async readWithQuery(input) {
        this.query = input.query;

        try {
            let records = await connections[this.connectionName].readWithQuery(this);
            let objects = this.convertResultsToDataObjects({results: records});
            return objects;
        } catch (error) {
            let newError =  `ReadWithQuery encountered an error: ${error}`;
            throw newError;
        }
    }

    /**
     * Retrieve several database entries, beginning with the startID, and continuing until there are no more records or the limit is reached.
     * @param {*} input
     * @param {*} input.startID The ID to begin retrieving records at.
     * @param {*} input.limit The maximum number of records to retrieve.
     * @returns An array of retrieved objects.
     * @throws An error if the database operation fails.
     */
    async readMany(input) {
        this.start = input.startID;
        this.limit = input.limit;

        try {
            let records = await connections[this.connectionName].readMany(this);
            let objects = this.convertResultsToDataObjects({results: records});
            return objects;
        } catch (error) {
            let newError = `ReadMany encountered an error: ${error}`;
            throw newError;
        }

    }

    /**
     * Get every record from the database that corresponds to the dataObject's table.
     * @returns An array of retrieved objects.
     * @throws An error if the database operation fails.
     */
    async readAll() {

        try {
            let records = await connections[this.connectionName].readAll(this);
            let objects = this.convertResultsToDataObjects({results: records});
            return objects;
        } catch (error) {
            let newError = `ReadAll encountered an error: ${error}`;
            throw newError;
        }

    }

    /**
     * Get the last record from the database that corresponds to the dataObject's table.
     * @returns A retrieved object.
     * @throws An error if the database operation fails.
     */
    async readLast() {

        try {
            let data = await connections[this.connectionName].readLast(this);
            this.data = data
            return this
        } catch (error) {
            let newError = `ReadLast encountered an error: ${error}`;
            throw newError;
        }
    }

    /**
     * Get the most recent records from the database that correspond to the dataObject's table.
     * @param {*} input.limit The maximum number of records to retrieve.
     * @returns An array of retrieved objects, sorted as the most recent record being first.
     * @throws An error if the database operation fails.
     */
    async readMostRecent(input) {
        this.limit = input.limit

        try {
            let records = await connections[this.connectionName].readMostRecent(this);
            let objects = this.convertResultsToDataObjects({results: records});
            return objects;
        } catch (error) {
            let newError = `readMostRecent encountered an error: ${error}`;
            throw newError;
        }
    }

    /**
     * Retrieve every database entry encountered when searching all fields for a given value.
     * @param {*} input
     * @param {*} input.value The value that must be found in a field.
     * @returns The retrieved objects.
     * @throws An error if the database operation fails.
     */
     async readAllWithAnyKeyHavingValue(input) {
        this.value = input.value;

        try {
            let records = await connections[this.connectionName].readAllWithAnyKeyHavingValue(this);
            let objects = this.convertResultsToDataObjects({results: records});
            return objects;
        } catch (error) {
            let newError = `readAllWithAnyKeyHavingValue encountered an error: ${error}`;
            throw newError;
        }
    }

    /**
     * Updates a database record to match the dataObject's internal state, using the dataObject's ID as the identifier.
     * @param input An object with keys that should match the dataObject's data definition.
     * @returns The updated object.
     * @throws An error if the database operation fails.
     */
    async update(input) {

        try {
            this.merge(input);
        } catch (error) {
            throw `There was an error with the merge operation on update: ${error}`;
        }

        try {
            let keys = Object.keys(this.dataTypesByKey);
            keys.map((key) => {
                if (this.dataTypesByKey[key] === "date") {
                    let data = this.data[key];
                    this.data[key] = new Date(data);
                }
            });
        } catch (error) {
            throw `An error occurred while update was converting fields to Date objects: ${error}`
        }

        let typeError = this.checkForTypeError();

        //Bail out.
        if (typeError) {
            throw typeError;
        }

        try {
            await connections[this.connectionName].update(this);
            return this;
        } catch (error) {
            let newError = `Update encountered an error: ${error}`;
            throw newError;
        }

    }

    /**
     * Remove the database record with the dataObject's ID.
     * @returns The deleted object.
     * @throws An error if the database operation fails.
     */
    async delete() {
        try {
            await connections[this.connectionName].delete(this);
            return this;
        } catch (error) {
            let newError = `Delete encountered an error: ${error}`;
            throw newError;
        }
    }

}

exports.DataObject = DataObject;
