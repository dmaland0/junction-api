/****************************************
*****************************************
Dependencies*/

//Builtin And Third-Party Modules

//Custom Modules

/****************************************
*****************************************
Module Logic*/

exports.databaseError = function(resultCallback, error) {
    resultCallback(false, 500, [], "A database error occurred.", error);
};

exports.noneFound = function(resultCallback, objectPlural) {
    resultCallback(false, 404, [], `No ${objectPlural} were found using the criteria specified.`, null);
};

exports.operationError = function(resultCallback, error) {
    resultCallback(false, 400, [], `An error occurred with an operation.`, error);
}
