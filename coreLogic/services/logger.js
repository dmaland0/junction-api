/****************************************
*****************************************
Dependencies*/

//Builtin And Third-Party Modules
const u = require("../utilities/jsUtilities").JSUtilities;
const fs = require('fs');
//Custom Modules
const DataObject = require("../services/dataObject").DataObject;
/****************************************
*****************************************
Module Logic*/

class LogEntry {

    /**
     * Create a new log entry.
     * @param {*} input Properties to set the data fields with.
     * @param {String} input.remoteHost The IP address of the connecting machine.
     * @param {Integer} input.remotePort The port in use on the connecting machine.
     * @param {String} input.headers The headers used in the connection.
     * @param {String} input.verb The HTTP verb used, if applicable.
     * @param {String} input.url The requested URL, if applicable.
     * @param {Integer} input.statusCode The HTTP status code, if applicable.
     * @param {String} input.createdAt The date code for when the log entry was created.
     */
    constructor(input = {}) {

        this.data = {
            remoteHost: "",
            remotePort: "",
            headers: "",
            verb: "",
            url: "",
            statusCode: "",
            createdAt: "",
            message: "",
            misc: ""
        }

        Object.keys(input).map((key) => {
            this.data[key] = input[key];
        });

        this.data.createdAt = new Date();

    }

    /**
     * Sets the data fields via a provided request object.
     * @param {Object} request A node HTTP request object.
     */
    async createFromRequest(request) {
        let data = this.data;
        data.remoteHost = request.connection.remoteAddress;
        data.remotePort = request.connection.remotePort;
        data.headers = JSON.stringify(request.headers).replace(/["{}]/gi, "").replace(/,/gi, ", ");
        data.verb = request.method;
        data.url = request.headers.host + request.url;
        data.statusCode = 0;
        data.createdAt = new Date();

        try {
            await this.create();
        } catch (error) {
            console.log(`\nWARNING: Logging problem encountered at ${new Date()}: ${error}\n`);
        }

    }

    /**
     * Sets the data fields via a provided response object.
     * @param {Object} request A node HTTP response object.
     * @param {Object} body A response body.
     */
    async createFromResponse(response, body) {
        let data = this.data;
        let jsonError = null;

        try {
            body = JSON.parse(body);
        } catch (error) {
            jsonError = error;
        }

        if (response.connection) {
            data.remoteHost = response.connection.remoteAddress;
            data.remotePort = response.connection.remotePort;
          } else {
            data.remoteHost = "Not Available";
            data.remotePort = 0;
          }

          data.headers = response._header.replace(/\r\n/gi, " ");
          data.verb = "N/A";
          data.url = "N/A";
          data.statusCode = response.statusCode;
          data.createdAt = new Date()

          if (!jsonError && body.message) {
              data.message = body.message
          }

          try {
            await this.create();
        } catch (error) {
            console.log(`\nWARNING: Logging problem encountered at ${new Date()}: ${error}\n`);
        }

    }

    async create(misc) {

        let time = this.data.createdAt;
        let timeComponents = [time.getFullYear(), time.getMonth()+1, time.getDate()];
        let timeString = "";

        this.data.misc = misc;

        timeComponents.map((component) => {
            if (component.toString().length < 2) {
                component = "0" + component.toString();
              }
              timeString = `${timeString}${component}`;
        });

        return new Promise((resolve, reject) => {
            fs.appendFile(`./serverLogs/${timeString}`, `${JSON.stringify(this.data, null, "\t")}\n`, 'utf8', (error) => {
                if (error) {
                    reject(error);
                } else {
                    resolve();
                }
            });
        });        
        
    }

    readOne(callObject, resultCallback) {
        let file = callObject.parameters[0];
        fs.readFile(`./serverLogs/${file}`,{encoding: 'utf-8'}, (error, data) => {
            if (error) {
                resultCallback(false, 500, [], "An error occurred while reading a log file.", error.message);
            } else {
                let logEntries = data.split("\n");

                let output = logEntries.map((entry) => {
                    let parsed;

                    try {
                        parsed = JSON.parse(entry)
                    } catch (error) {
                        return error;
                    }
                    return parsed;
                });

                resultCallback(true, 200, [], "A log file was read.", output);
            }
        });
    }

}

exports.LogEntry = LogEntry;
