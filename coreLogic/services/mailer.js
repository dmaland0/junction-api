/****************************************
*****************************************
Dependencies*/

//Builtin And Third-Party Modules
const fs = require("fs");
const u = require("../utilities/jsUtilities").JSUtilities;
//Custom Modules

//Dependent Modules
const mailgun = null

/****************************************
*****************************************
Module Logic*/

/**
  @param data.from  the address that an email should be seen as coming from
  @param data.to  the destination address for the email
  @param data.subject  the email subject
  @param data.text the email body

@callback 
 @callback boolean, whether the operation was successful 
 @callback string, an error message if necessary
 */
exports.sendMail = function(data, callback) {

  function logEmail(data, returnCall) {

    var logString = "\n==========" + u.makeSQLDatetime();
    var parsedData = u.iterableParser(data);

    u.forIn(parsedData, function(index, string) {
      logString += "\n" + string;
    });

    logString += "\n==========";

    fs.appendFile("./logEmails", logString, function(error) {

      var message = null;

      if (error) {

        message = "An error occurred while trying to write to logEmails: " + error;
        returnCall(message);

      } else {

        message = "Email data was written to logEmails.";
        returnCall(message);

      }

    });

  }

  if (!mailgun) {

    logEmail(data, function(message) {

      if (message.indexOf("error") >= 0) {
        callback(false, message);
      } else {
        callback(true, message);
      }

    });

  } else {

    mailgun.messages().send(data, function (error, body) {

        if (error) {
          var parsedError = u.iterableParser(error);
          var errorMessage = "";

          u.forIn(parsedError, function(index, value) {
            errorMessage += value + " ";
          });

          logEmail(data, function(error) {
            errorMessage += error;
            callback(false, errorMessage);
          });

        } else {
          callback(true);
          return body;
        }

    });

  }

};
