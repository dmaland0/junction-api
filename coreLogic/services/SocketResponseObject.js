/**
 * A structured, outbound communication for WebSocket interactions.
 */
class SocketResponseObject {
    /**
     * Constructor
    * @param {Boolean} input.success
    * @param {Int} input.code
    * @param {String} input.message
    * @param {Multiple} input.data
     */
  constructor (input) {
    this.success = input.success;
    this.code = input.code;
    this.message = input.message;
    this.data = input.data;
  }
}

exports.instance = SocketResponseObject;
