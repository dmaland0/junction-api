/****************************************
*****************************************
Dependencies*/

//Builtin And Third-Party Modules
const u = require("../coreLogic/utilities/jsUtilities").JSUtilities;
//Custom Modules
const DataObject = require("../coreLogic/services/dataObject").DataObject;
const connections = require("../coreLogic/services/connections").connections;
/****************************************
*****************************************
Module Logic*/

class Example extends DataObject {

    /**
     * Create a new Example dataObject.
     * @param {*} input Properties to set the data fields with.
     * @param {Integer} input.id The desired ID.
     * @param {String} input.message A string.
     */
    constructor(input = {}) {

        super({
            connectionName: "example",
            table: "examples",
            messageIdentifier: "Example",
            data: {
                id: 0,
                message: ""
            },
            dataTypesByKey: {
            }
        });

        let keys = Object.keys(this.data);

        keys.map((key) => {
            this.dataTypesByKey[key] = typeof(this.data[key])
        });

        let inputKeys = Object.keys(input)

        inputKeys.map((inputKey) => {
            this.data[inputKey] = input[inputKey];
        });

    }

}

exports.Example = Example;
